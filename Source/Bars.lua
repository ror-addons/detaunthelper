DetauntHelperBar = Frame:Subclass ("DetauntHelperBar");

function DetauntHelperBar:Create( i )
    local barname;
    if i<10 then
        barname = "DetauntHelperBarTemplate0"..i;
    else
        barname = "DetauntHelperBarTemplate"..i;
    end
    local bar = self:CreateFromTemplate (barname, "Root");
    bar.m_BarName = barname;
    bar:UpdateSettings();
    bar:SetShow(false);
    return bar;
end

function DetauntHelperBar:UpdateSettings()
    DetauntBarSettings.UpdateBarSettings( self.m_BarName );
end

function DetauntHelperBar:SetShow(flag)
    WindowSetShowing(self.m_BarName , flag);
end

function DetauntHelperBar:AnchorTo(anchorframe)
    WindowClearAnchors( self.m_BarName ); 
    WindowAddAnchor( self.m_BarName, "bottomleft", anchorframe,"topleft", 0,1 );
end

function DetauntHelperBar:GetBarName()
    return self.m_BarName;
end

function DetauntHelperBar:SetTargetDamage(val)
    self.m_TargetDamage = val;
    LabelSetText( self.m_BarName .. "TargetDamage", towstring(""..val) );
end

function DetauntHelperBar:GetTargetDamage(val)
    return self.m_TargetDamage;
end

function DetauntHelperBar:SetTargetName(name)
    self.m_TargetName = name;
    LabelSetText( self.m_BarName .. "TargetName", towstring(string.sub(name,1,10)) );
end

function DetauntHelperBar:GetTargetName(name)
    return self.m_TargetName;
end

function DetauntHelperBar:SetThreatColor( level )
    WindowSetTintColor(self.m_BarName .. "Background", 255, level , 0);
end

function DetauntHelperBar:UpdateTargetIcon()
    local pinfo = DetauntTargetInfo.GetEnemyData(self.m_TargetName);
    icon,dimX,dimY = pinfo:GetCareerIconData();
    local texture,tx,ty = GetIconData(icon);
    DynamicImageSetTexture( self.m_BarName .. "ClassIcon", texture, tx, ty );
    DynamicImageSetTextureScale(self.m_BarName .. "ClassIcon",1);
    DynamicImageSetTextureDimensions (self.m_BarName .. "ClassIcon", dimX, dimY);  
    WindowSetShowing(self.m_BarName .. "ClassIcon", true);
end

function DetauntHelperBar:OnMouseWheel (flags, mouseX, mouseY)
    local skiptarget = DetauntHelper.CurrentTargetName == self.m_TargetName;
    if self.m_TargetName~=nil then
        self:ButtonHandler( 3, skiptarget);
    end
end

function DetauntHelperBar:OnLButtonDown (flags, mouseX, mouseY)
    local skiptarget = DetauntHelper.CurrentTargetName == self.m_TargetName;
    if self.m_TargetName~=nil then
        local actionId = 0;
        local actionType = GameData.PlayerActions.NONE;
        if skiptarget then
            actionId,actionType = DetauntAbilityManager.GetAbilityForFlag(flags);
        end
        WindowSetGameActionTrigger (self.m_BarName, actionId);
        WindowSetGameActionData (self.m_BarName, actionType, actionId, L"");
        self:ButtonHandler( 2, skiptarget);
    end
end

function DetauntHelperBar:OnRButtonDown (flags, mouseX, mouseY)
    local skiptarget = DetauntHelper.CurrentTargetName == self.m_TargetName;
    if self.m_TargetName~=nil then
        self:ButtonHandler( 4, skiptarget);
    end
end

--Handle a detaunt bar being clicked in a generic way
function DetauntHelperBar:ButtonHandler( setting, skiptarget )
    local target_name = self.m_TargetName;
    local settings = DetauntHelperConfig.GetCurrentConfig();
    if settings.TargetOn == setting then
        if not skiptarget then
            DTHCommon.Print(DHLang.GetString(DHStrings.TARGET_MSG)..StringToWString(target_name));
            DetauntHelper.TargetPlayer(target_name);
        end
    end
    if settings.MonitorOn == setting then
        DetauntHelperMonitor.CreateWindow(target_name);
    end
    if settings.ClearOn == setting then
        DetauntHelper.Reset();
    end
end