DetauntHelper = {};
if DetauntHelper.DetauntHelperSettings == nil then
   DetauntHelper.DetauntHelperSettings = {};
end
local VariableSettings = DetauntHelper.DetauntHelperSettings;

--Detaunt Lookup Table
local DetauntLookup = {};
DetauntLookup[9256] = true; -- Disapating Hatred
DetauntLookup[9265] = true; -- Walk Between Worlds
DetauntLookup[8162] = true; -- SmokeScreen
DetauntLookup[9555] = true; -- Terrifying Vision
DetauntLookup[1516] = true; -- Addling Shot
DetauntLookup[8477] = true; -- Horrifying Visions
DetauntLookup[1595] = true; -- Rune of Preservation
DetauntLookup[9089] = true; -- Distracting Shot
DetauntLookup[1918] = true; -- Look Over There
DetauntLookup[9474] = true; -- Dread Aspect
DetauntLookup[1827] = true; -- Dont Eat Me
DetauntLookup[8245] = true; -- Repent
DetauntLookup[9392] = true; -- Enchanting Beauty
DetauntLookup[8088] = true; -- Get Thee Behind Me
DetauntLookup[8621] = true; -- Chaotic Blur
--CareerID to detaunt
CareerID2Detaunt = {};
CareerID2Detaunt[103] = 9256; -- Disapating Hatred
CareerID2Detaunt[62]  = 8162; -- SmokeScreen
CareerID2Detaunt[106] = 9555; -- Terrifying Vision
CareerID2Detaunt[23]  = 1516; -- Addling Shot
CareerID2Detaunt[67]  = 8477; -- Horrifying Visions
CareerID2Detaunt[22]  = 1595; -- Rune of Preservation
CareerID2Detaunt[101] = 9089; -- Distracting Shot
CareerID2Detaunt[26]  = 1918; -- Look Over There
CareerID2Detaunt[107] = 9474; -- Dread Aspect
CareerID2Detaunt[27]  = 1827; -- Dont Eat Me
CareerID2Detaunt[63]  = 8245; -- Repent
CareerID2Detaunt[105] = 9392; -- Enchanting Beauty
CareerID2Detaunt[60]  = 8088; -- Get Thee Behind Me
CareerID2Detaunt[66]  = 8621; -- Chaotic Blur

--delay for updating
local TIME_DELAY = 1;

--remaining time for dealy
local timeLeft = TIME_DELAY;

--are we in combat?
local inCombat = false;

--time since combat
local sinceCombat = 0;

--Hold settings data
local DHSLocal;

--Addon namespace
DetauntHelper = {}

--Holds information on when a player/mob joined combat
--would be nice if we could capture death events to clear this
DetauntHelper.StartList = {};

--holds the current target
DetauntHelper.CurrentTargetName = "None";

--Holds the detaunt target
DetauntHelper.DetauntTargetName = nil;

--holds a bool indicating if TortallDPSCore updated
DetauntHelper.TortallDPSCoreUpdated = false;

--target command function
function DetauntHelper.TargetPlayer(playerName)
  DetauntHelper.DetauntTargetName = playerName;
  if DetauntHelper.CurrentTargetName == DetauntHelper.DetauntTargetName then
      if DHSLocal["SoundOn"] then
         PlaySound(DHSLocal["TargetAcquiredSound"]);
      end
  end
  SystemData.UserInput.ChatText = towstring("/target "..playerName)
  BroadcastEvent(SystemData.Events.SEND_CHAT_TEXT)
end

--Handle Reset on Detaunt Cast
function DetauntHelper.OnPlayerBeginCast( abilityId, isChannel, desiredCastTime, averageLatency )
    if DetauntLookup[abilityId]==true and DHSLocal.ResetOnDetaunt then
        local abilityData = Player.GetAbilityData(abilityId);
        if abilityData ~= nil then
            DetauntHelper.DetauntTimer = abilityData.cooldown;
        end
        DetauntHelper.Reset();
    end
end

--Init
function DetauntHelper.Initialize()
    RegisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "DetauntHelper.OnInterfaceLoaded" )
    RegisterEventHandler( SystemData.Events.ENTER_WORLD, "DetauntHelper.OnInterfaceLoaded" )
    DetauntHelper.bars = {};
    DHSLocal = DetauntHelperConfig.Initialize();
    DetauntTargetInfo.Initialize();    
    DetauntHelper.SetupWindows();
    LayoutEditor.RegisterWindow( "DetauntHelperAnchorFrame", DHLang.GetString(DHStrings.LAYOUT_ANCHOR), DHLang.GetString(DHStrings.LAYOUT_ANCHOR),
      	                             false, false, true, nil );
    LayoutEditor.RegisterWindow( "DetauntHelperIcon", DHLang.GetString(DHStrings.LAYOUT_ICON), DHLang.GetString(DHStrings.LAYOUT_ICON),
      	                             false, false, true, nil );
    TortallDPSCore.Register("DetauntHelper", DetauntHelper.DamageUpdate, DetauntHelper.DamageNewObject, nil, "DetauntHelper");
    RegisterEventHandler (SystemData.Events.PLAYER_TARGET_UPDATED, "DetauntHelper.OnTargetChange");
    RegisterEventHandler( SystemData.Events.PLAYER_BEGIN_CAST, "DetauntHelper.OnPlayerBeginCast" );
    DetauntHelperMonitor.Initialize();  
end

--Holds if we have registered or not
DetauntHelper.HasRegistered = false;

--sets up optional dependencies
function DetauntHelper.OnInterfaceLoaded()
    DetauntHelperNerf.Initialize();
    if not DetauntHelper.HasRegistered then
        DetauntHelper.HasRegistered = true;
        if LibSlash~=nill and LibSlash.RegisterWSlashCmd~=nil then
            LibSlash.RegisterWSlashCmd("dh", function(input) DetauntHelper.HandleSlash(input) end); 
        end
    end
    UnregisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "DetauntHelper.OnInterfaceLoaded" )
    UnregisterEventHandler( SystemData.Events.ENTER_WORLD, "DetauntHelper.OnInterfaceLoaded" )
end

--Sets up the detaunt bars
function DetauntHelper.SetupWindows()
   local anchorframe = "DetauntHelperAnchorFrame";
   for i=1,DHSLocal.TargCount,1 do
      local bar;
      if DetauntHelper.bars[i]==nil then
         bar = DetauntHelperBar:Create(i);
         bar:AnchorTo(anchorframe);
         bar:UpdateSettings();
         DetauntHelper.bars[i] = bar;
      else
         bar = DetauntHelper.bars[i];
         bar:UpdateSettings();
         bar:SetShow(false);
      end
      anchorframe = bar:GetBarName();
   end
end

--Print a help message
function DetauntHelper.PrintHelp()
    DTHCommon.Print(DHLang.GetString(DHStrings.DH_COMMANDS),false);
    DTHCommon.Print(DHLang.GetString(DHStrings.DH_TARGTOP),false);
    DTHCommon.Print(DHLang.GetString(DHStrings.DH_TARGTOP_EX),false);
    DTHCommon.Print(DHLang.GetString(DHStrings.DH_RESET),false);
    DTHCommon.Print(DHLang.GetString(DHStrings.DH_RESET_EX),false);
    for k,v in pairs(DetauntHelper.Commands) do
        DTHCommon.Print(towstring("/dh "..k..":")..v.desc,false);
    end
end

--Hold commands
DetauntHelper.Commands = {};

--Process slash commands
function DetauntHelper.AddCommand(command,description,fctn)
    DetauntHelper.Commands[command] = {fn =fctn, desc=description};
end

--Process slash commands
function DetauntHelper.HandleSlash(input)
    if not input or tostring(input):gsub(" ","") == "" then
        DetauntHelper.PrintHelp()
        return
    end
    local command, tail = input:match(L"([a-z]+)[ ]?(.*)")
    if command~=nil then
        command = tostring(command)
        local cmdObj = DetauntHelper.Commands[command];
        if cmdObj and cmdObj.fn then
           if cmdObj.fn(command,tail) then
               return;
           end
        end
    else
       command = ""   
    end
    DTHCommon.Print("Detaunt Helper:Unknown Command:'"..command.."'",true);
    DetauntHelper.PrintHelp()
end

--Function registered with TortalDPS library to get new object updates
function DetauntHelper.DamageNewObject(dmgType, entry, entryType, channel, data)
    DetauntHelper.TortallDPSCoreUpdated = true;
    if entryType == TortallDPSCore.TARGET and dmgType == TortallDPSCore.DAMAGE_TAKEN then
        local name = WStringToString(entry.Name);
        local deaths = DetauntTargetInfo.GetEnemyData(name):Get("deaths");
        DetauntHelper.StartList[name] = { t_Start = DetauntHelper.CombatTime, t_Since = DetauntHelper.CombatTime,
                                          v_Amount = 0, b_Expired = false, v_Old = 0, v_Deaths = deaths};
        if DHSLocal.PermaWatched[name]~=nil then
             DetauntHelperMonitor.CreateWindow( name );
        end
    end
end

--Function registered with TortalDPS library to get damage updates
function DetauntHelper.DamageUpdate( damage, healing, combatTime, channel )
   DetauntHelper.LastUpdate = damage;
   DetauntHelper.CombatTime = combatTime;
end

--Sort through targets recieved from Tortal
function DetauntHelper.GetTargetList()
    local LL = {};
    local value;
    local tname;
    local ttime;
    local amount;
    if DetauntHelper.LastUpdate == nil or DetauntHelper.LastUpdate.Taken == nil then
        return LL;
    end
    local enemy;
    local ctime = DetauntHelper.CombatTime;
    for k,v in pairs(DetauntHelper.LastUpdate.Taken.Object) do
       tname = WStringToString(k);
       enemy = DetauntHelper.StartList[tname];
       if enemy == nil then
           continue;
       end
       local pinfo = DetauntTargetInfo.GetEnemyData(tname);
       local deaths = pinfo:Get('deaths');
       if deaths~=enemy["v_Deaths"] then
           enemy["b_Expired"] = true;
           enemy["v_Old"] = v.Amount;
           enemy["v_Amount"] = v.Amount;
           enemy["v_Deaths"] = deaths;
       elseif (v.Amount == enemy["v_Amount"]) then
           local threshtime = enemy["t_Since"]+DHSLocal.Threshold;
           if threshtime<=ctime then
               enemy["b_Expired"] = true;
               enemy["v_Old"] = v.Amount;
               continue;
           end
       else
           enemy["t_Since"] = ctime;
           enemy["v_Amount"] = v.Amount;
           if enemy["b_Expired"] then
               enemy["t_Start"] = ctime;
               enemy["v_Old"] = v.Amount;
           end
           enemy["b_Expired"] = false;
       end
       ttime = ctime - (enemy['t_Start'] or 0);
       ttime = ttime or 1;
       amount = math.floor( (v.Amount-enemy["v_Old"]) / ttime );
       --somehow I get NaNs in here
       if amount<=0 or amount~=amount or math.abs(amount)==math.huge then
           continue;
       end
       pinfo:UpdateDPS( amount );
       enemy["v_DPS"] = amount;
       LL[1+#LL] = {name = tname, value = amount };
    end
    table.sort( LL, function(a,b) return a.value > b.value; end);
    return LL;
end

--Script function to target the top of the list
function DetauntHelper.TargetTop()
    local LL = DetauntHelper.GetTargetList();
    if #LL>0 then
        local target_name = LL[1].name;
        DTHCommon.Print(DHLang.GetString(DHStrings.TARGET_MSG)..StringToWString(target_name));
        DetauntHelper.TargetPlayer(target_name);
    end
end

--Function that will make some noise if we have targeted our
--intended detaunt target
function DetauntHelper.OnTargetChange(classification, targetId, targetType)
    TargetInfo:UpdateFromClient()
    if targetType == 5 then
        DetauntTargetInfo.CreateEntryFromHostileTargetInfo(TargetInfo.m_Units[classification]); 
    end
    if classification == "selfhostiletarget" and DetauntHelper.lastTargetId ~= targetId then
        local name = (WStringToString(TargetInfo:UnitName(classification))):match("([^^]+)^?([^^]*)");
        DetauntHelper.lastTargetId = targetId;
        if not name then
            DetauntHelper.CurrentTargetName = "None";
            return
        end
        DetauntHelper.CurrentTargetName = name;
        if name == DetauntHelper.DetauntTargetName then
            if DHSLocal["SoundOn"] then
               PlaySound(DHSLocal["TargetAcquiredSound"]);
            end
        end
    end
end

--Update the bars
function DetauntHelper.UpdateDetauntBars()
   local LL = DetauntHelper.GetTargetList();
   local LLlen = #LL;
   local increment = 255/DHSLocal.TargCount;
   for i=1,DHSLocal.TargCount,1 do
      local bar = DetauntHelper.bars[i];
      if i<=LLlen then
         bar:SetTargetName( LL[i].name );
         bar:SetTargetDamage( LL[i].value );
         bar:SetThreatColor( math.floor((i-1)*increment) );
         bar:UpdateTargetIcon();
         bar:SetShow(true);
      else
         bar:SetShow(false);
      end
   end
end

--Hanlde update events
function DetauntHelper.OnUpdate( elapsed )
    if ( GameData.Player.inCombat ) then
        inCombat = true;
    end
    timeLeft = timeLeft - elapsed
    if timeLeft > 0 then
        return
    end
    local sinceLastUpdate = TIME_DELAY - timeLeft;
    DetauntHelperConfig.UpdateTime(sinceLastUpdate);
    if DetauntHelper.DetauntTimer ~= nil then
        DetauntHelper.DetauntTimer = DetauntHelper.DetauntTimer - sinceLastUpdate;
        if DetauntHelper.DetauntTimer <= 0 then
            DetauntHelper.DetauntTimer = nil;
            if DHSLocal["SoundOn"] then
                PlaySound(DHSLocal["DetauntReadySound"]);
            end
        end
    end
    timeLeft = TIME_DELAY;
    DetauntTargetInfo.UpdateTargetInformation();
    if inCombat then
        sinceCombat = 0
        DetauntHelper.UpdateDetauntBars();
        DetauntHelperMonitor.UpdateWindows( );
        inCombat = false;
    else
        sinceCombat = sinceCombat + sinceLastUpdate
        if sinceCombat > 4 then
            sinceCombat = 0
            DetauntHelper.UpdateReset();
        end
    end
    DetauntTargetInfo.CleanCache();
end

--Reset everything

function DetauntHelper.UpdateReset(...)
    if DetauntHelper.TortallDPSCoreUpdated then
        DetauntHelper.Reset();
    end
end

function DetauntHelper.Reset(...)
    DetauntHelper.DetauntTargetName = nil;
    DetauntHelper.StartList = {};
    DetauntHelper.TortallDPSCoreUpdated = false;
    TortallDPSCore.Reset("DetauntHelper");
    DetauntHelper.LastUpdate = nil;
    DetauntHelper.CombatTime = 0;
    for k,v in pairs(DetauntHelper.bars) do
        v:SetShow(false);
    end
end

