local DHTarget = {};

local TargetData;

local TargetDataDefaults = {
    careerId = 0,
    rank = "?",
    deaths = 0
};

DetauntTargetInfo = {};

--Determins if scenario data needs updating
DetauntTargetInfo.ScenarioHasUpdated = false;
--Determins if scenario is running
DetauntTargetInfo.InScenario = false;
--Holds Scenario Participants
DetauntTargetInfo.ScenarioParticipants = {};

function DetauntTargetInfo.PackDB()
    local data = DetauntHelper.TargetData;
    local lData = {}
    local lSort = {}
    i = 1;
    for k,v in pairs(TargetData) do
        if type(v.name)~="wstring" then
            v.name = StringToWString(v.name.."^M");
        end
        lData[i] = v;
        lSort[i] = i;
        i = i+ 1;
    end
    local max_records = DetauntHelperConfig.GetCurrentConfig().MaximumRecords;
    local qty_records = #lData;
    EA_ChatWindow.Print( DHLang.GetString(DHStrings.RECS_IN_DB)..StringToWString(tostring(qty_records)) );
    if qty_records <= max_records then
        EA_ChatWindow.Print( DHLang.GetString(DHStrings.RECS_CLEANED)..L"0");
        return;
    end
    local cleaned = qty_records - max_records;
    local index;
    local sortfunc = DetauntTarget.sortButtonData[5].sort;
    local old_direction = DetauntTarget.sortButtonData[5].direction;
    DetauntTarget.sortButtonData[5].direction = DetauntTarget.BUTTON_DOWN;
    DetauntTarget.displayData = lData;
    DetauntTarget.sortData = lSort;
    table.sort(DetauntTarget.sortData, sortfunc);
    for _,index in ipairs(DetauntTarget.sortData) do
        DetauntHelper.TargetData[DTHCommon.CleanName(lData[index].name)]=nil;
        qty_records = qty_records - 1;
        if qty_records <= max_records then
            break;
        end
    end
    DetauntTarget.sortButtonData[5].direction = old_direction;   
    EA_ChatWindow.Print( DHLang.GetString(DHStrings.RECS_CLEANED)..StringToWString(tostring(cleaned)) );
end

DHTarget.Cache = {};

function DHTarget:UpdateInfo(pinfo)
    if pinfo.careerLine~=nil then
        self.pinfo.careerLine = pinfo.careerLine;
    end
    if pinfo.careerId ~= 0 then
        self.pinfo.careerId = pinfo.careerId;
    end
    if pinfo.rank ~= nil then
        self.pinfo.rank = pinfo.rank;
    end
end

function DHTarget:New(arg0)
    local name;
    local oinfo;
    local argType = type(arg0);
    if argType=="table" then
        oinfo = arg0;
        name = oinfo.name;
    elseif argType=="string" then
        name = StringToWString(arg0.."^M");
    else
        name = arg0;
    end
    local sName = DTHCommon.CleanName(name);
    local o = DHTarget.Cache[sName];
    if o == nill then
        o = {};
        setmetatable(o,self);
        self.__index = self;
        o.sName = sName;
        o.pinfo = TargetData[o.sName] or { name = name };
        DTHCommon.CopyAtoBIfNil(TargetDataDefaults,o.pinfo);
        o.careerId = o.pinfo.careerId;
        DHTarget.Cache[o.sName] = o;
    end
    if oinfo~=nil then
        o:UpdateInfo(oinfo)
    end
    o.touched = DetauntHelperConfig.GetTime();
    return o;
end

function DHTarget:Dispose()
    DHTarget.Cache[self.sName]=nil;
end

function DHTarget:Delete()
    TargetData[self.sName] = nil;
end

function DHTarget:Save()
    if self.pinfo.careerId ~= 0 or self.pinfo.careerLine~=nil then
        local tdate = GetTodaysDate();
        local pinfo = self.pinfo;
        pinfo.month = tdate.todaysMonth;
        pinfo.day = tdate.todaysDay;
        pinfo.year = tdate.todaysYear;
        TargetData[self.sName] = pinfo;
    end
end

function DHTarget:Get(attr)
    local value = self.pinfo[attr] or TargetDataDefaults[attr];
    return value;
end

function DHTarget:GetDPS()
    local min_dps = self.pinfo.min_dps or 0;
    local max_dps = self.pinfo.max_dps or 0;
    local avg_dps = self.pinfo.avg_dps or 0;
    return min_dps,avg_dps,max_dps
end

function DHTarget:UpdateDPS(dps)
    local min_dps = self.pinfo.min_dps or dps;
    local max_dps = self.pinfo.max_dps or dps;
    local avg_dps = self.pinfo.avg_dps or dps;
    avg_dps = (avg_dps + dps)/2;
    if dps>max_dps then
        max_dps = dps;
    elseif dps<min_dps then
        min_dps = dps;
    end
    self.pinfo.max_dps = max_dps;
    self.pinfo.min_dps = min_dps;
    self.pinfo.avg_dps = avg_dps;
end

function DHTarget:Set(attr,value)
    self.pinfo[attr] = value;
end

function DHTarget:_GetCareerIcon()
    if self.pinfo.careerId == 0 then
        if self.pinfo.careerLine ~= nil then
            self.careerIcon = Icons.GetCareerIconIDFromCareerLine(self.pinfo.careerLine);
        else
            if (string.find(self.sName," ") ~=nil) then
                self.careerIcon = 310
            else            
                self.careerIcon = 51;
            end
        end
    else
        self.careerIcon = Icons.GetCareerIconIDFromCareerNamesID(self.pinfo.careerId);
    end
    self.careerId = self.pinfo.careerId;
end

function DHTarget:GetCareerName()
    local className;
    if self.pinfo.careerId == 0 and self.pinfo.careerLine~=nil then
        className = GetCareerLine(self.pinfo.careerLine, 1);
    else
        className = GetCareerName(self.pinfo.careerId, 1 );
    end
    return className;
end

function DHTarget:HasIconUpdate()
    return ((self.careerId ~= self.pinfo.careerId) or (self.careerIcon == nil)); 
end

function DHTarget:GetCareerIconData()
    if self:HasIconUpdate() then
        self:_GetCareerIcon();
    end
    local dimX;
    local dimY;
    if self.careerIcon == 51 or self.careerIcon == 310 then
        dimX = 64;
        dimY = 64;
    else
        dimX = 32;
        dimY = 32;
    end
    return self.careerIcon,dimX,dimY;
end

function DetauntTargetInfo.AddTarget(classification)
    if not DetauntTargetInfo.InScenario then
        local unit = TargetInfo.m_Units[classification];
        tinfo = {};
        tinfo.name = unit.name;
        tinfo.careerId = unit.career;
        tinfo.rank = unit.level;
        tinfo.deaths = 0;
        DetauntTargetInfo.GetEnemyData(tinfo):Save();
    end
end

function DetauntTargetInfo.Initialize()
    RegisterEventHandler( SystemData.Events.SCENARIO_BEGIN, "DetauntTargetInfo.UpdateScenarioInfo");
    RegisterEventHandler( SystemData.Events.SCENARIO_UPDATE_POINTS, "DetauntTargetInfo.UpdateScenarioInfo");
    RegisterEventHandler( SystemData.Events.SCENARIO_PLAYERS_LIST_UPDATED, "DetauntTargetInfo.UpdateScenarioInfo");
    RegisterEventHandler( SystemData.Events.SCENARIO_PLAYERS_LIST_STATS_UPDATED, "DetauntTargetInfo.UpdateScenarioInfo");
    RegisterEventHandler( SystemData.Events.SCENARIO_END, "DetauntTargetInfo.EndUpdateEnemyData" );
    RegisterEventHandler( SystemData.Events.PLAYER_AREA_CHANGED,"DetauntTargetInfo.EmptyCache");
    local ccfg = DetauntHelperConfig.GetCurrentConfig();
    if DetauntHelper.TargetData == nil then
        DetauntHelper.TargetData = {};
    end
    TargetData = DetauntHelper.TargetData;
    DetauntTarget.UpdateTargetData();
end


function DetauntTargetInfo.GetDPS(pinfo)
    local min_dps = pinfo.min_dps or 0;
    local max_dps = pinfo.max_dps or 0;
    local avg_dps = pinfo.avg_dps or 0;
    return min_dps,avg_dps,max_dps
end


function DetauntTargetInfo.GetCareerIconDataFromCareerLine(cid)
    local icon = Icons.GetCareerIconIDFromCareerLine(cid);
    return icon,32,32;
end

function DetauntTargetInfo.GetCareerIconDataFromCareerID(cid)
    local dimX;
    local dimY;
    local icon;
    if cid==0 then
        icon = 51;
    else
        icon = Icons.GetCareerIconIDFromCareerNamesID(cid);
    end
    if icon == 51 then
        dimX = 64;
        dimY = 64;
    else
        dimX = 32;
        dimY = 32;
    end
    return icon,dimX,dimY;
end

function DetauntTargetInfo.EmptyCache()
    DHTarget.Cache = {};
end

local CACHE_TTL = 300;

function DetauntTargetInfo.CleanCache()
    local diposelist = {};
    for k,v in pairs(DHTarget.Cache) do
        if (DetauntHelperConfig.GetTime()-v.touched) > CACHE_TTL then
            diposelist[1+#diposelist]=k;
        end
    end
    for _,k in ipairs(diposelist) do
        DHTarget.Cache[k]=nil;
    end
end

--Updates player data if in a scenario
--Got this from theseeker
function DetauntTargetInfo.UpdateScenarioInfo()
    DetauntTargetInfo.ScenarioHasUpdated = true;
end

function DetauntTargetInfo.CleanUp()
    local d_list = {};
    for k,v in pairs(TargetData) do
        if Icons.GetCareerIconIDFromCareerNamesID(v.careerId)==0 then
            v.careerLine = v.careerId;
            v.careerId = 0;
        end
    end
end


function DetauntTargetInfo.IsInScenario(playerData)
	local sName = DTHCommon.CleanName(playerData.name);
	return DetauntTargetInfo.ScenarioParticipants[sName]~=nil
end

function DetauntTargetInfo.CreateEntryFromHostileTargetInfo(ti)
    tinfo = {};
    tinfo.name = ti.name;
    tinfo.careerId = 0;
    tinfo.careerLine = ti.career;
    tinfo.rank = ti.level;
    if( GameData.Player.realm == GameData.Realm.ORDER ) then
        tinfo.realm = GameData.Realm.DESTRUCTION;
    else
        tinfo.realm = GameData.Realm.ORDER;
    end
    tinfo.deaths = 0;
    local pInfo = DetauntTargetInfo.GetEnemyData(tinfo);
    pInfo:Save();
end

function DetauntTargetInfo._UpdateScenarioInfo()
    local interimPlayerData = GameData.GetScenarioPlayers()
    local tinfo;
    DetauntTargetInfo.ScenarioParticipants = {};
    if( interimPlayerData ~= nil ) then        
        for key, value in ipairs( interimPlayerData ) do
            if value.realm == GameData.Player.realm then
                continue;
            end
            tinfo = {};
            tinfo.name = value.name;
            tinfo.careerId = value.careerId;
            tinfo.rank = value.rank;
            tinfo.realm = value.realm;
            tinfo.solokills = value.solokills;
            tinfo.groupkills= value.groupkills;
            tinfo.renown = value.renown;
            tinfo.deaths = value.deaths;
            tinfo.damagedealt = value.damagedealt;
            tinfo.deathblows = value.deathblows;
            tinfo.healingdealt = value.healingdealt;
            tinfo.renownbonus = value.renownbonus;
            tinfo.experience = value.experience;
            local pInfo = DetauntTargetInfo.GetEnemyData(tinfo);
            DetauntTargetInfo.ScenarioParticipants[pInfo.sName]=true;
            pInfo:Save();
        end        
    end    
end

function DetauntTargetInfo.UpdateTargetInformation()
    if DetauntTargetInfo.ScenarioHasUpdated then
        DetauntTargetInfo._UpdateScenarioInfo();
        DetauntTargetInfo.ScenarioHasUpdated = false;
    end
end

--Stops updating scenario data
function DetauntTargetInfo.EndUpdateEnemyData()
    DetauntTargetInfo.ScenarioHasUpdated = false;
    DetauntTargetInfo.InScenario = false;
    DetauntTargetInfo.ScenarioParticipants = {}
end

function DetauntTargetInfo.GetEnemyData(shortName)
    return DHTarget:New(shortName);
end
