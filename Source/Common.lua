DTHCommon = {};

function DTHCommon.CleanName(name)
    if type(name)=="wstring" then
        return WStringToString(name):match("([^^]+)^?([^^]*)");
    end
    return name;
end

--printing function
function DTHCommon.Print(...)
    msg,doboth = ...;
    msg = towstring(msg);
    if doboth==nil then
        doboth = true;
    end
    if AlertTextWindow~=nil and doboth then
        AlertTextWindow.AddLine(SystemData.AlertText.Types.DEFAULT, msg);
    end
    EA_ChatWindow.Print(msg);
end

function DTHCommon.NameSpace(str)
    local root = DetauntHelper;
    mstr = "(%a%w*)[\.]?";
    if type(str)=="wstring" then
        mstr = StringToWString(mstr);
    end
    local each
    for each in string.gmatch(str,mstr) do
        if root[each]==nil then
            root[each]={};
        end
        root = root[each];
    end
    return root;
end

--Handle updating the addons settings
function DTHCommon.CopyAtoBIfNil(a,b)
    for k,v in pairs(a) do
       if b[k] == nil then
          b[k] = v;
       end
    end
end