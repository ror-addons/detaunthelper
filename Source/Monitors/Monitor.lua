DetauntHelperMonitor = {};

local dthm_windowName = "DetauntHelperMonitor";

DetauntHelperMonitor.monitored ={}

function DetauntHelperMonitor.DestroyWindow(...)
    local winName = string.gsub(SystemData.MouseOverWindow.name,"Close","");
    local targname = string.gsub( winName, dthm_windowName, "");
    DetauntHelperMonitor.monitored[targname]=nil;
    DestroyWindow(winName);
end


function DetauntHelperMonitor.Initialize( )
    DetauntHelperMonitor.monitored = {};
    LayoutEditor.RegisterWindow( "DetauntHelper_MonitorAnchorFrame", DHLang.GetString(DHStrings.LAYOUT_MONITOR), DHLang.GetString(DHStrings.LAYOUT_MONITOR),
      	                             false, false, true, nil );
end


function DetauntHelperMonitor.GetAnchorData(  )
    local anchor = {};
    anchor["relativeAnchorPoint"]="topleft";
    anchor["anchorPoint"]="topleft";
    anchor["parent"]="DetauntHelper_MonitorAnchorFrame";
    anchor["x"]=0;
    anchor["y"]=0;
    return anchor;
end

function DetauntHelperMonitor.CreateWindow( shortName )
    local windowName = dthm_windowName..shortName;
    if ( not DoesWindowExist( windowName ) ) then	
        CreateWindowFromTemplate( windowName, "DetauntHelper_MonitorTemplate", "Root" );		
        WindowSetDimensions( windowName , 165, 140);
        local settings = DetauntHelperConfig.GetCurrentConfig();
        local anchor = settings.PermaWatched[shortName];
        if anchor==nill then
            anchor = DetauntHelperMonitor.GetAnchorData(  );
        end
        WindowAddAnchor( windowName, anchor.anchorPoint, anchor.parent, anchor.relativeAnchorPoint, anchor.x, anchor.y );	
        DynamicImageSetTextureDimensions (windowName .. "Eye", 24, 24);	
        DetauntHelperMonitor.monitored[shortName]=true;
    end	
    DetauntHelperMonitor.UpdateWindow( shortName )    
end

function DetauntHelperMonitor.UpdateWindows( )
    for shortName,_ in pairs( DetauntHelperMonitor.monitored ) do
        DetauntHelperMonitor.UpdateWindow( shortName );
    end
end

function DetauntHelperMonitor.GetPlayerData( shortName )
    local sinfo = DetauntHelper.StartList[shortName];
    local dps_val = "?";
    if sinfo~=nil then
        dps_val = sinfo["v_DPS"];
    end
    local pinfo = DetauntTargetInfo.GetEnemyData(shortName);
    return pinfo,dps_val;
end

function DetauntHelperMonitor.UpdateWindow( shortName )
    local windowName = dthm_windowName..shortName;    
    if ( DoesWindowExist( windowName ) ) then
        local pinfo,dps_val = DetauntHelperMonitor.GetPlayerData( shortName );	
        local icon,dimX,dimY = pinfo:GetCareerIconData();
        local className = pinfo:GetCareerName();
        local texture,tx,ty = GetIconData(icon);
        DynamicImageSetTexture( windowName .. "DataIcon", texture, tx, ty );
        DynamicImageSetTextureScale(windowName .. "DataIcon",1);
        DynamicImageSetTextureDimensions (windowName .. "DataIcon", dimX, dimY);
        LabelSetText( windowName .. "DataName", StringToWString(string.sub(shortName,1,10)) );
        LabelSetText( windowName .. "DataCareer", className );
        LabelSetText( windowName .. "DataLevel", DHLang.GetString(DHStrings.LABEL_RANK)..StringToWString(": "..(pinfo:Get("rank"))) );
        LabelSetText( windowName .. "DataDPS", DHLang.GetString(DHStrings.LABEL_DPS)..StringToWString(": "..dps_val) );
        if DetauntHelperConfig.GetCurrentConfig().PermaWatched[shortName]~=nil then
            texture,tx,ty = GetIconData(108);
        else
            texture,tx,ty = GetIconData(160);
        end
        DynamicImageSetTexture( windowName .. "Eye", texture, tx, ty );
    end
end

function DetauntHelperMonitor.OnMonitorClicked()
    local targname = string.gsub( SystemData.MouseOverWindow.name, dthm_windowName, "");
    DTHCommon.Print(  DHLang.GetString(DHStrings.TARGET_MSG)..StringToWString(targname) );
    DetauntHelper.TargetPlayer(targname);
end

function DetauntHelperMonitor.OnEyeLButtonUp()
    local activeWindow = SystemData.ActiveWindow.name;
    local match_str = dthm_windowName.."(.+)Eye";
    local player = activeWindow:match(match_str);
    local texture,tx,ty;
    local settings = DetauntHelperConfig.GetCurrentConfig();
    if settings.PermaWatched[player]~=nil then
        settings.PermaWatched[player]=nil;
        texture,tx,ty = GetIconData(160);
    else
        local relativeAnchorPoint, anchorPoint, parent, x, y = WindowGetAnchor( dthm_windowName..player, 1 );
        local anchor = {};
        anchor["relativeAnchorPoint"]=relativeAnchorPoint;
        anchor["anchorPoint"]=anchorPoint;
        anchor["parent"]=parent;
        anchor["x"]=x;
        anchor["y"]=y;
        settings.PermaWatched[player]=anchor;
        texture,tx,ty = GetIconData(108);
    end
    DynamicImageSetTexture( activeWindow, texture, tx, ty );
    DynamicImageSetTextureDimensions (activeWindow , 24, 24);
end

function DetauntHelperMonitor.OnLButtonUp()
    local player = string.gsub( SystemData.MouseOverWindow.name, dthm_windowName, "");
    local texture,tx,ty;
    local settings = DetauntHelperConfig.GetCurrentConfig();
    if settings.PermaWatched[player]~=nil then
        local relativeAnchorPoint, anchorPoint, parent, x, y = WindowGetAnchor( dthm_windowName..player, 1 );
        local anchor = {};
        anchor["relativeAnchorPoint"]=relativeAnchorPoint;
        anchor["anchorPoint"]=anchorPoint;
        anchor["parent"]=parent;
        anchor["x"]=x;
        anchor["y"]=y;
        settings.PermaWatched[player]=anchor;
    end
end
