DetauntBarSettings = {};
local _windowID = "DTHConfigWindow";
local _BarSettingswindowID = _windowID.."BaseWindow2";
local VariableSettings;
local _buttonLookup = {_BarSettingswindowID.."BarPropertiesShowIcon",_BarSettingswindowID.."BarPropertiesShowLabel",_BarSettingswindowID.."BarPropertiesShowDPS"};
local LEFT_LABEL_OFFSET = 10;
local POSSIBLE_PLABEL_WIDTH = 180;
local _comboSettings = DHLang.GetMouseComboSettings();
local soundNameLookup;
local soundValueLookup;
local dth_font_names = DHLang.GetFontNames();
--Set a combobox from a list of items
local function ComboBoxSetItemsFromList( v_windowName, settingsList )
	ComboBoxClearMenuItems(v_windowName);
	for i, v in ipairs( settingsList ) do
		ComboBoxAddMenuItem( v_windowName , v );
	end
end
--Set the basic combo settings
local function ComboBoxSetItems( v_windowName, settingsList )
	ComboBoxSetItemsFromList(v_windowName,_comboSettings);
end


function DetauntBarSettings.UpdateBarSettings(t_BarName,doCreate)
	--Create Bar
	if doCreate then
		CreateWindowFromTemplate( t_BarName, "DetauntHelperBar", "Root" );
	end
	--Local variables
	local labelframe = t_BarName.."TargetName";
	local dmgframe = t_BarName.."TargetDamage";
	local iconframe = t_BarName.."ClassIcon";
	--Set Font
	local fontval = DetauntHelperConfig.Fonts[VariableSettings.BarSettings.Font];
	LabelSetFont( labelframe, fontval,WindowUtils.FONT_DEFAULT_TEXT_LINESPACING );
	LabelSetFont( dmgframe, fontval,WindowUtils.FONT_DEFAULT_TEXT_LINESPACING );
	local LabelDimensions = VariableSettings.BarSettings.LabelDimensions;
	local DamageDimensions = VariableSettings.BarSettings.DamageDimensions;
	local WindowDimensions = VariableSettings.BarSettings.WindowDimensions;
	local iconwidth = 0;
	--Set Icon
	if VariableSettings.BarSettings.BarProperties[1] then
		iconwidth = WindowDimensions[2];
	end
	WindowSetDimensions(iconframe,iconwidth,WindowDimensions[2]);
	--Set Target Label	
	WindowSetDimensions(labelframe,LabelDimensions[1],LabelDimensions[2]);
	--Set Damage Label
	WindowSetDimensions(dmgframe,DamageDimensions[1],DamageDimensions[2]);
	--Set Window Dimensions
	WindowSetDimensions(t_BarName,WindowDimensions[1],WindowDimensions[2]);
	--Setup Anchors
	WindowClearAnchors( iconframe );
	WindowClearAnchors( dmgframe );
	WindowClearAnchors( labelframe );
	WindowAddAnchor( iconframe, "left", t_BarName,"left", 0,0 );
	WindowAddAnchor( labelframe, "right", iconframe,"left", LEFT_LABEL_OFFSET,0 );
	WindowAddAnchor( dmgframe, "right", labelframe,"left", 0,0 );
	WindowSetScale(t_BarName,0.1 + 0.9*(VariableSettings.BarSettings.ScaleBar));
end


function DetauntBarSettings.GetSoundsIndex()
	local TargetAcquiredSoundIndex;
	local DetauntReadySoundIndex;
	for i,v in ipairs(soundValueLookup) do
		if v == VariableSettings.TargetAcquiredSound then
			TargetAcquiredSoundIndex = i;
		elseif v == VariableSettings.DetauntReadySound then
			DetauntReadySoundIndex = i;
		end
	end
	return TargetAcquiredSoundIndex,DetauntReadySoundIndex
end

function DetauntBarSettings.InitUI()
	VariableSettings = DetauntHelper.DetauntHelperSettings
    soundNameLookup = {};
    soundValueLookup = {};
    for k,v in pairs(GameData.Sound) do
        soundNameLookup[1+#soundNameLookup] = StringToWString(k);
        soundValueLookup[1+#soundValueLookup] = v;
    end
    --Set up tab name
    ButtonSetText( _windowID.."TabsTab2", DHLang.GetString(DHStrings.TAB_DISPLAY) )
    --Setup Bar Settings Window
    --Mouse Behavior Labels
    LabelSetText( _BarSettingswindowID.."MouseTitle", DHLang.GetString(DHStrings.CLICK_BEHAVIOR) );
    LabelSetText( _BarSettingswindowID.."MouseTargetOnLabel", DHLang.GetString(DHStrings.CLICK_TARGET_ON));
    LabelSetText( _BarSettingswindowID.."MouseClearOnLabel", DHLang.GetString(DHStrings.CLICK_CLEAR_ON));
    LabelSetText( _BarSettingswindowID.."MouseMonitorOnLabel", DHLang.GetString(DHStrings.CLICK_MONITOR_ON) );
    --Mouse Behavior Combos
    for _,v in ipairs( {"TargetOn","ClearOn","MonitorOn"} ) do
        ComboBoxSetItems(_BarSettingswindowID.."Mouse"..v.."ClickCombo");
    end
    --Miscellaneous Labels
    LabelSetText( _BarSettingswindowID.."MiscellaneousTitle",DHLang.GetString(DHStrings.MISC) );
    LabelSetText( _BarSettingswindowID.."MiscellaneousBQEditBoxLabel",DHLang.GetString(DHStrings.BAR_QTY));
    LabelSetText( _BarSettingswindowID.."MiscellaneousMREditBoxLabel",DHLang.GetString(DHStrings.REC_QTY));
    LabelSetText( _BarSettingswindowID.."MiscellaneousDelayEditBoxLabel",DHLang.GetString(DHStrings.EXP_DELAY));
    LabelSetText( _BarSettingswindowID.."MiscellaneousDetauntRefreshLabel", DHLang.GetString(DHStrings.DO_RESET) );
    --Bar Font Label
    LabelSetText( _BarSettingswindowID.."BarFontsTitle", DHLang.GetString(DHStrings.FONT_STYLE) );
    LabelSetText( _BarSettingswindowID.."BarFontsFontStyleLabel", DHLang.GetString(DHStrings.FONT_TYPE) );
    LabelSetText( _BarSettingswindowID.."BarFontsFontColorLabel", DHLang.GetString(DHStrings.FONT_COLOR) );
    --Bar Font Combos
    ComboBoxSetItemsFromList(_BarSettingswindowID.."BarFontsFontStyleCombo",dth_font_names);
    ComboBoxSetItemsFromList(_BarSettingswindowID.."BarFontsFontColorCombo",DHLang.GetFontColors());
    --Bar Properties Labels
    LabelSetText( _BarSettingswindowID.."BarPropertiesTitle", DHLang.GetString(DHStrings.ITEMS_SHOWN) );
    LabelSetText( _BarSettingswindowID.."BarPropertiesShowIconLabel",DHLang.GetString(DHStrings.ITEMS_SHOWN_ICON) );
    LabelSetText( _BarSettingswindowID.."BarPropertiesShowLabelLabel", DHLang.GetString(DHStrings.ITEMS_SHOWN_TARG) );
    LabelSetText( _BarSettingswindowID.."BarPropertiesShowDPSLabel", DHLang.GetString(DHStrings.ITEMS_SHOWN_DPS) );	
    --BarSize Label
    LabelSetText( _BarSettingswindowID.."BarSizeTitle",DHLang.GetString(DHStrings.BAR_SIZE) );
    LabelSetText( _BarSettingswindowID .. "BarSizeWidthLabel", DHLang.GetString(DHStrings.NAME_WIDTH) );
    LabelSetText( _BarSettingswindowID .. "BarSizeHeightLabel", DHLang.GetString(DHStrings.BAR_HEIGHT) );
    LabelSetText( _BarSettingswindowID .. "BarSizeScaleLabel", DHLang.GetString(DHStrings.BAR_SCALE) );
    --Sound Notifications Labels
    LabelSetText( _BarSettingswindowID .. "SoundNotificationsSoundOnLabel", DHLang.GetString(DHStrings.SOUND_ENABLED) );
    LabelSetText( _BarSettingswindowID .. "SoundNotificationsTitle", DHLang.GetString(DHStrings.SOUND_NOTICE) );
    LabelSetText( _BarSettingswindowID .. "SoundNotificationsOnDetauntLabel", DHLang.GetString(DHStrings.SOUND_TARG) );
    LabelSetText( _BarSettingswindowID .. "SoundNotificationsOnDetauntReadyLabel", DHLang.GetString(DHStrings.SOUND_RDY) );
    --Sound Notifications Combos
    ComboBoxSetItemsFromList(_BarSettingswindowID.."SoundNotificationsOnDetauntCombo",soundNameLookup);
    ComboBoxSetItemsFromList(_BarSettingswindowID.."SoundNotificationsOnDetauntReadyCombo",soundNameLookup);
    --Setup Example Window
    local barname = _BarSettingswindowID.."BarSizeBS_Example";
    local iconframe = barname.."ClassIcon";
    WindowClearAnchors( iconframe );
    WindowAddAnchor( iconframe, "left", barname,"left", 0,0 );
    local targetlabel = barname.."TargetName";
    WindowClearAnchors( targetlabel );
    local damagelabel = barname.."TargetDamage";
    WindowClearAnchors( damagelabel );
    WindowAddAnchor( damagelabel, "right", barname .. "TargetName","left", 0,0 );
    local texture,tx,ty = GetIconData(Icons.GetCareerIconIDFromCareerNamesID(GameData.Player.career.id));
    DynamicImageSetTexture( barname .. "ClassIcon", texture, tx, ty );
    DynamicImageSetTextureScale(barname .. "ClassIcon",1);
    DynamicImageSetTextureDimensions (barname .. "ClassIcon", 32, 32); 
    LabelSetText( barname .. "TargetName", StringToWString(string.sub(DTHCommon.CleanName(GameData.Player.name),1,10)) );
    WindowAddAnchor( targetlabel, "right", barname.."ClassIcon","left", LEFT_LABEL_OFFSET,0 );
    LabelSetText( barname .. "TargetDamage", towstring("9999") ); 
    WindowSetShowing(barname .. "ClassIcon", true);
end

function DetauntBarSettings.ShowUI()
	--Get Font
	local fontval = DetauntHelperConfig.Fonts[VariableSettings["Font"]];
	--Setup Size Sliders
	SliderBarSetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_ScaleSliderBar",  VariableSettings.BarSettings["ScaleBar"]);
	SliderBarSetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_WidthSliderBar", VariableSettings.BarSettings["WidthBar"] );
	SliderBarSetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_HeightSliderBar", VariableSettings.BarSettings["HeightBar"] );
	--Setup Miscellaneous
	TextEditBoxSetText( _BarSettingswindowID.."MiscellaneousDelayEditBox",StringToWString(""..(VariableSettings.Threshold) ) );
	TextEditBoxSetText( _BarSettingswindowID.."MiscellaneousBQEditBox",StringToWString(""..(VariableSettings.TargCount) ) );
	TextEditBoxSetText( _BarSettingswindowID.."MiscellaneousMREditBox",StringToWString(""..(VariableSettings.MaximumRecords) ) );
	ButtonSetPressedFlag( _BarSettingswindowID.."MiscellaneousDetauntRefreshButton",  VariableSettings.ResetOnDetaunt);
	--Setup Mouse Combos
	for _,v in ipairs( {"TargetOn","ClearOn","MonitorOn"} ) do       
		ComboBoxSetSelectedMenuItem( _BarSettingswindowID.."Mouse"..v.."ClickCombo", VariableSettings[v]);
	end
	--Setup Font Combos
	ComboBoxSetSelectedMenuItem(_BarSettingswindowID.."BarFontsFontStyleCombo",VariableSettings.BarSettings["Font"]);
	ComboBoxSetSelectedMenuItem(_BarSettingswindowID.."BarFontsFontColorCombo",VariableSettings.BarSettings["FontColor"]);
	--Setup Bar Properties CheckBoxes
	ButtonSetPressedFlag( _BarSettingswindowID.."BarPropertiesShowIconButton",VariableSettings.BarSettings.BarProperties[1] );
	ButtonSetPressedFlag( _BarSettingswindowID.."BarPropertiesShowLabelButton",VariableSettings.BarSettings.BarProperties[2] );
	ButtonSetPressedFlag( _BarSettingswindowID.."BarPropertiesShowDPSButton",VariableSettings.BarSettings.BarProperties[3] );
	--Setup Example
	local barname = _BarSettingswindowID.."BarSizeBS_Example";
	LabelSetFont( barname .. "TargetName", fontval,WindowUtils.FONT_DEFAULT_TEXT_LINESPACING );
	LabelSetFont( barname .. "TargetDamage", fontval,WindowUtils.FONT_DEFAULT_TEXT_LINESPACING );
	local color = 255 * VariableSettings["FontColor"]-1;
	LabelSetTextColor(barname .. "TargetName",color,color,color);
	LabelSetTextColor(barname .. "TargetDamage",color,color,color);
	WindowSetDimensions(_BarSettingswindowID.."BarSizeBS_Example",VariableSettings.BarSettings["WindowDimensions"][1],VariableSettings.BarSettings["WindowDimensions"][2]);
	--update sound settings
	local TargetAcquiredSoundIndex,DetauntReadySoundIndex = DetauntBarSettings.GetSoundsIndex();
	ButtonSetPressedFlag( _BarSettingswindowID.."SoundNotificationsSoundOnButton",  VariableSettings.SoundOn);
	ComboBoxSetSelectedMenuItem(_BarSettingswindowID.."SoundNotificationsOnDetauntCombo",TargetAcquiredSoundIndex);
	ComboBoxSetSelectedMenuItem(_BarSettingswindowID.."SoundNotificationsOnDetauntReadyCombo",DetauntReadySoundIndex);		
	DetauntBarSettings.OnUpdateFontCombo();	
end

function DetauntBarSettings.Save()
	local TargCount = tonumber(TextEditBoxGetText( _BarSettingswindowID.."MiscellaneousBQEditBox"));
	if TargCount ~=nil then
		VariableSettings.TargCount = TargCount
	end
	local MaximumRecords = tonumber(TextEditBoxGetText( _BarSettingswindowID.."MiscellaneousMREditBox"));
	if TargCount ~=nil then
		VariableSettings.MaximumRecords = MaximumRecords
	end
	local Threshold = tonumber(TextEditBoxGetText( _BarSettingswindowID.."MiscellaneousDelayEditBox"));
	if Threshold ~=nil then
		VariableSettings.Threshold = Threshold
	end
	for _,v in ipairs( {"TargetOn","ClearOn","MonitorOn"} ) do        
		local comboIndex = ComboBoxGetSelectedMenuItem( _BarSettingswindowID.."Mouse"..v.."ClickCombo" );           
		VariableSettings[v] = comboIndex;
	end
	VariableSettings.BarSettings["Font"] = ComboBoxGetSelectedMenuItem(_BarSettingswindowID.."BarFontsFontStyleCombo");
	VariableSettings.BarSettings["FontColor"] = ComboBoxGetSelectedMenuItem(_BarSettingswindowID.."BarFontsFontColorCombo");
	local player_w,player_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_ExampleTargetName");
	local window_w,window_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_Example");
	local damage_w,damage_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_ExampleTargetDamage");
	VariableSettings.BarSettings["ScaleBar"] = SliderBarGetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_ScaleSliderBar");
	VariableSettings.BarSettings["WidthBar"] = SliderBarGetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_WidthSliderBar");
	VariableSettings.BarSettings["HeightBar"] = SliderBarGetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_HeightSliderBar");
	VariableSettings.BarSettings["LabelDimensions"] = {player_w,player_h};
	VariableSettings.BarSettings["DamageDimensions"] = {damage_w,damage_h};
	VariableSettings.BarSettings["WindowDimensions"] = {window_w,window_h};
	VariableSettings["TargetAcquiredSound"] = DetauntBarSettings.GetSoundForCombo(_BarSettingswindowID.."SoundNotificationsOnDetauntCombo");
	VariableSettings["DetauntReadySound"] = DetauntBarSettings.GetSoundForCombo(_BarSettingswindowID.."SoundNotificationsOnDetauntReadyCombo");
	DetauntHelper.SetupWindows();
end

function DetauntBarSettings.OnSoundNotificationSoundOnButton(...)
    local activeWindow = SystemData.ActiveWindow.name;
    local buttonName = activeWindow.."Button";
    local checked = not ButtonGetPressedFlag(buttonName);
    VariableSettings.SoundOn = checked;
    ButtonSetPressedFlag( buttonName, checked );
end

function DetauntBarSettings.OnDetauntRefreshSettingButton(...)
    local activeWindow = SystemData.ActiveWindow.name;
    local buttonName = activeWindow.."Button";
    local checked = not ButtonGetPressedFlag(buttonName);
    VariableSettings.ResetOnDetaunt = checked;
    ButtonSetPressedFlag( buttonName, checked );
end


function DetauntBarSettings.OnBarPropertyButton(...)
	local activeWindow = SystemData.ActiveWindow.name;
	for i,v in ipairs(_buttonLookup) do
		if tostring(activeWindow) == v then
			VariableSettings.BarSettings.BarProperties[i] = not VariableSettings.BarSettings.BarProperties[i];
		end
	end
	ButtonSetPressedFlag( _BarSettingswindowID.."BarPropertiesShowIconButton",VariableSettings.BarSettings.BarProperties[1] );
	ButtonSetPressedFlag( _BarSettingswindowID.."BarPropertiesShowLabelButton",VariableSettings.BarSettings.BarProperties[2] );
	ButtonSetPressedFlag( _BarSettingswindowID.."BarPropertiesShowDPSButton",VariableSettings.BarSettings.BarProperties[3] );
	DetauntBarSettings.OnUpdateFontCombo();
end

function DetauntBarSettings.OnScaleSlide( pos )
	local barname = _BarSettingswindowID.."BarSizeBS_Example";
	local newsize = 0.1 + 0.9*SliderBarGetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_ScaleSliderBar" );
	if(WindowGetScale(barname)~=newsize) then
		WindowSetScale(barname,newsize);
	end
end

function DetauntBarSettings.OnWidthSlide( pos )
	local barname = _BarSettingswindowID.."BarSizeBS_ExampleTargetName";
	local parentbar = _BarSettingswindowID.."BarSizeBS_Example";
	local newsize = POSSIBLE_PLABEL_WIDTH*SliderBarGetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_WidthSliderBar" );
	local w,h = WindowGetDimensions(barname);
	local pw,ph = WindowGetDimensions(parentbar);
	if(w~=newsize) then
		DetauntBarSettings.AdjustPlayerLabelWidth( );
	end
end

function DetauntBarSettings.OnHeightSlide( pos )
	local barname = _BarSettingswindowID.."BarSizeBS_ExampleClassIcon";
	local parentbar = _BarSettingswindowID.."BarSizeBS_Example";
	local player_w,player_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_ExampleTargetName");
	local window_w,window_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_Example");
	local damage_w,damage_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_ExampleTargetDamage");
	local possible_height = 48-damage_h;
	local newsize = possible_height*SliderBarGetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_HeightSliderBar" );
	if newsize < 0 then
		newsize = 0
	end
	newsize = newsize + damage_h;
	if(w~=newsize) then
		WindowSetDimensions(barname,newsize,newsize);
		WindowSetDimensions(parentbar,newsize+2*LEFT_LABEL_OFFSET+player_w+damage_w,newsize);
		DetauntBarSettings.AdjustPlayerLabelWidth( );
	end
end

function DetauntBarSettings.OnUpdateCombo( ... )
end

function DetauntBarSettings.OnUpdateFontCombo( ... )
	local barname = _BarSettingswindowID.."BarSizeBS_Example";
	local fontval = DetauntHelperConfig.Fonts[ComboBoxGetSelectedMenuItem(_BarSettingswindowID.."BarFontsFontStyleCombo")];
	LabelSetFont( barname .. "TargetName", fontval,WindowUtils.FONT_DEFAULT_TEXT_LINESPACING );
	LabelSetFont( barname .. "TargetDamage", fontval,WindowUtils.FONT_DEFAULT_TEXT_LINESPACING );
	local x,y = LabelGetTextDimensions(barname .. "TargetDamage");
	x = x+10;
	if not VariableSettings.BarSettings.BarProperties[3] then
		x = 0;
	else
		x = 64.857147216797;
	end
	local w,h = WindowGetDimensions(barname .. "TargetDamage");
	WindowSetDimensions(barname .. "TargetDamage",x,h);
	DetauntBarSettings.AdjustPlayerLabelWidth( );
end


function DetauntBarSettings.OnDetauntSelChanged()
	local box = _BarSettingswindowID.."SoundNotificationsOnDetauntCombo";
	PlaySound(DetauntBarSettings.GetSoundForCombo(box));
end

function DetauntBarSettings.OnDetauntReadySelChanged()
	local box = _BarSettingswindowID.."SoundNotificationsOnDetauntReadyCombo";
	PlaySound(DetauntBarSettings.GetSoundForCombo(box));
end

function DetauntBarSettings.GetSoundForCombo(box)
	local index = ComboBoxGetSelectedMenuItem(box);
	return soundValueLookup[index];
end

function DetauntBarSettings.OnUpdateFontColorCombo( ... )
	local barname = _BarSettingswindowID.."BarSizeBS_Example";
	local color = 255 * (ComboBoxGetSelectedMenuItem(_BarSettingswindowID.."BarFontsFontColorCombo")-1);
	LabelSetTextColor(barname .. "TargetName",color,color,color);
	LabelSetTextColor(barname .. "TargetDamage",color,color,color);	
end

function DetauntBarSettings.GetPlayerLabelWidth( )
	local player_w,player_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_ExampleTargetName");
	local window_w,window_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_Example");
	local damage_w,damage_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_ExampleTargetDamage");
	if not VariableSettings.BarSettings.BarProperties[1] then
		window_h = 0;
	end
	POSSIBLE_PLABEL_WIDTH = 320-(window_h+2*LEFT_LABEL_OFFSET+damage_w);
	local player_w = POSSIBLE_PLABEL_WIDTH*SliderBarGetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_WidthSliderBar" );
	if not VariableSettings.BarSettings.BarProperties[2] then
		player_w = 0;
	end
	return player_w,player_h
end

function DetauntBarSettings.GetWindowWidth( )
	local player_w,player_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_ExampleTargetName");
	local window_w,window_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_Example");
	local damage_w,damage_h = WindowGetDimensions(_BarSettingswindowID.."BarSizeBS_ExampleTargetDamage");
	local width = player_w + 2*LEFT_LABEL_OFFSET + damage_w;
	if VariableSettings.BarSettings.BarProperties[1] then
		width = width + window_h;	
	end
	if (not VariableSettings.BarSettings.BarProperties[2]) and (not VariableSettings.BarSettings.BarProperties[3]) then
		width = window_h;
	end
	return width;
end

function DetauntBarSettings.AdjustPlayerLabelWidth( )
	local barname = _BarSettingswindowID.."BarSizeBS_ExampleTargetName";
	local parentbar = _BarSettingswindowID.."BarSizeBS_Example";
	local player_w,player_h = DetauntBarSettings.GetPlayerLabelWidth( );
	local pw,ph = WindowGetDimensions(parentbar);
	WindowSetDimensions(barname,player_w,player_h);
	local iconwidth = ph;
	if not VariableSettings.BarSettings.BarProperties[1] then
		iconwidth = 0;
	end
	WindowSetDimensions(_BarSettingswindowID.."BarSizeBS_ExampleClassIcon",iconwidth,ph);
	WindowSetDimensions(parentbar,DetauntBarSettings.GetWindowWidth( ),ph);
	WindowSetScale(parentbar,DetauntBarSettings.GetScale( ));
end


function DetauntBarSettings.GetScale( )
	local barname = _BarSettingswindowID.."BarSizeBS_Example";
	return 0.1 + 0.9*SliderBarGetCurrentPosition( _BarSettingswindowID .. "BarSizeBS_ScaleSliderBar" );
end