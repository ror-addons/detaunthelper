DetauntAbilityManager = {};
local DTCACfg;

local _windowID = "DTHConfigWindow";
local _AbilitiesWindow = _windowID.."BaseWindow3";
local _AbilitiesWindows = {};
_AbilitiesWindows["0"]    = L"NONE";
_AbilitiesWindows["4"]    = L"SHIFT";
_AbilitiesWindows["8"]    = L"CTRL";
_AbilitiesWindows["32"]   = L"ALT";
_AbilitiesWindows["12"]   = L"SHIFT+CTRL";
_AbilitiesWindows["36"]   = L"SHIFT+ALT";
_AbilitiesWindows["40"]   = L"ALT+CTRL";
_AbilitiesWindows["44"]   = L"SHIFT+ALT+CTRL";


local SPELL_DEFAULTS = {};
SPELL_DEFAULTS["0"]    = 0;
SPELL_DEFAULTS["4"]    = 0;
SPELL_DEFAULTS["8"]    = 0;
SPELL_DEFAULTS["32"]   = 0;
SPELL_DEFAULTS["12"]   = 0;
SPELL_DEFAULTS["36"]   = 0;
SPELL_DEFAULTS["40"]   = 0;
SPELL_DEFAULTS["44"]   = 0;

function DetauntAbilityManager.InitUI()
    local VariableSettings = DetauntHelper.DetauntHelperSettings
    if VariableSettings.SpellSettings == nil then
        VariableSettings.SpellSettings = {}
    end
    DTCACfg = VariableSettings.SpellSettings;
    DTHCommon.CopyAtoBIfNil(SPELL_DEFAULTS,DTCACfg);
    ButtonSetText( _windowID.."TabsTab3", DHLang.GetString(DHStrings.ABILITY) );
end

function DetauntAbilityManager.ShowUI()
    local k,v;
    local texture,tx,ty;
    for k,v in pairs(_AbilitiesWindows) do
        local abilityId,abilityType,handled = DetauntAbilityManager.GetAbilityForFlag(tonumber(k));
        if not handled then
            local abilityData = GetAbilityData(DTCACfg[k]);
            texture,tx,ty = GetIconData(abilityData.iconNum);
            DynamicImageSetTextureDimensions(_AbilitiesWindow..k.."Icon", 64,64 );
        else
            texture = "dhnbicon"
            tx = 0
            ty = 0
            DynamicImageSetTextureDimensions(_AbilitiesWindow..k.."Icon", 16,16 );
        end
        LabelSetText( _AbilitiesWindow..k.."Label", v );  
        DynamicImageSetTexture( _AbilitiesWindow..k.."Icon", texture, tx, ty );      
    end
end

--Need to look into GameData.PlayerActions.DO_MACRO
--See ActionButton:CursorSwap (flags, x, y)
function DetauntAbilityManager.OnDrop(flags, x, y)
    if Cursor.IconOnCursor() then
        local ability = Player.GetAbilityData (Cursor.Data.ObjectId)
        if ability == nil or ability.abilityType == GameData.AbilityType.TACTIC then
            return;
        end
        local iconWindow = SystemData.MouseOverWindow.name;
        local abilityId = Cursor.Data.ObjectId
        Cursor.Clear ()
        local setting_flags = iconWindow:match(_AbilitiesWindow.."(%d+)Icon");
        if abilityId ~= nil then
            DTCACfg[setting_flags]=abilityId;
            local abilityData = GetAbilityData(abilityId);
            local texture,tx,ty = GetIconData(abilityData.iconNum);
            DynamicImageSetTexture( iconWindow, texture, tx, ty );
        end
    end
end

function DetauntAbilityManager.OnClear()
    local iconWindow = SystemData.MouseOverWindow.name;
    local setting_flags = iconWindow:match(_AbilitiesWindow.."(%d+)Icon");
    local abilityId = 0;
    DetauntHelperNerf.NerfDel(tostring(setting_flags));
    DTCACfg[setting_flags]=abilityId;
    local abilityData = GetAbilityData(abilityId);
    local texture,tx,ty = GetIconData(abilityData.iconNum);
    DynamicImageSetTexture( iconWindow, texture, tx, ty );
end

function DetauntAbilityManager.OnMouseover()
    local iconWindow = SystemData.MouseOverWindow.name;
    local setting_flags = iconWindow:match(_AbilitiesWindow.."(%d+)Icon");
    local abilityId = DTCACfg[setting_flags];
    if abilityId ~=0 then
        local abilityData = GetAbilityData(abilityId);
        local text = AbilitiesWindow.AbilityTypeDesc[ AbilitiesWindow.currentMode ];
        Tooltips.CreateAbilityTooltip( abilityData, SystemData.ActiveWindow.name, AbilitiesWindow.TOOLTIP_ANCHOR, text );
    end
end

function DetauntAbilityManager.GetAbilityForFlag(flag)
    local key = tostring(flag);
    local abilityId,actionType,handled = DetauntHelperNerf.GetAbilityForFlag(flag);
    if not handled then
        local pAbilityId = DTCACfg[key];
        if pAbilityId~=nil then
            abilityId = pAbilityId;
            actionType = GameData.PlayerActions.DO_ABILITY;
        end
    end
    return abilityId,actionType,handled;
end