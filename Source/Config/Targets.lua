DetauntTarget = {};
local VariableSettings;

local MAX_VISIBLE_ROWS = 6;
local _windowID = "DTHConfigWindow";
local _TargetwindowID = _windowID.."BaseWindow1";

DetauntTarget.Filters = {};
DetauntTarget.displayData = {};
DetauntTarget.sortData = {};
DetauntTarget.BUTTON_DOWN = true;
DetauntTarget.BUTTON_UP = false;
DetauntTarget.sortButtonData = {};
DetauntTarget.FIELDS = { 'rank','name','class','avg_dps','seen' };

function DetauntTarget.ShowUI()
end

function DetauntTarget.InitUI()
    VariableSettings = DetauntHelper.DetauntHelperSettings;
	ButtonSetText( _windowID.."TabsTab1", DHLang.GetString(DHStrings.TAB_TARGETS) )
	--Target Grid Settings
	ButtonSetText( _TargetwindowID.."SortButton1", DHLang.GetString(DHStrings.LABEL_RANK))
	ButtonSetText( _TargetwindowID.."SortButton2", DHLang.GetString(DHStrings.NAME))
	ButtonSetText( _TargetwindowID.."SortButton3", DHLang.GetString(DHStrings.CLASS))
	ButtonSetText( _TargetwindowID.."SortButton4", DHLang.GetString(DHStrings.ITEMS_SHOWN_DPS))
	ButtonSetText( _TargetwindowID.."SortButton5", DHLang.GetString(DHStrings.LAST_SEEN))
	ButtonSetText( _TargetwindowID.."SortButton6", DHLang.GetString(DHStrings.MONITOR))
	--Target Button Stuff
	LabelSetText( _TargetwindowID .. "LevelRangeTitle", DHLang.GetString(DHStrings.LABEL_RANK) );
	LabelSetText( _TargetwindowID .. "LevelRangeDash", L"-" );	
	LabelSetText( _TargetwindowID .. "DPSRangeTitle", DHLang.GetString(DHStrings.LABEL_DPS) );
	LabelSetText( _TargetwindowID .. "DPSRangeDash", L"-" );
	LabelSetText( _TargetwindowID .. "SeenRangeTitle", DHLang.GetString(DHStrings.LAST_SEEN) );
	LabelSetText( _TargetwindowID .. "SeenRangeDash", L"-" );	
	LabelSetText( _TargetwindowID .. "NameTitle", DHLang.GetString(DHStrings.NAME) );
	LabelSetText( _TargetwindowID .. "ClassTitle", DHLang.GetString(DHStrings.CLASS) );
	LabelSetText( _TargetwindowID .. "MonitorTitle", DHLang.GetString(DHStrings.MONITOR) );
	LabelSetText( _TargetwindowID .. "MonitorOnLabel",DHLang.GetString(DHStrings.ACTIVE) );
	LabelSetText( _TargetwindowID .. "MonitorOffLabel",DHLang.GetString(DHStrings.INACTIVE) );
	ButtonSetText( _TargetwindowID .. "Apply", GetString( StringTables.Default.LABEL_APPLY ) )
	ButtonSetText( _TargetwindowID .. "Reset", GetString( StringTables.Default.LABEL_RESET ) )
	ButtonSetText( _TargetwindowID .. "Pack", DHLang.GetString(DHStrings.PACK_DB) )
	LabelSetText( _TargetwindowID .. "ScenarioLabel", DHLang.GetString(DHStrings.SCENARIO) );
	LabelSetTextColor(_TargetwindowID .. "ScenarioLabel", 0, 255, 0);--255, 204, 102);
	LabelSetFont(_TargetwindowID .. "ScenarioLabel", "font_default_sub_heading", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING);
	ButtonSetDisabledFlag(_TargetwindowID .. "ScenarioButton", true);
end

local function sorter( index )
	local attr = DetauntTarget.FIELDS[index];
	local function sortfunc( ai, bi)
		local a = DetauntTarget.displayData[ai];
		local b = DetauntTarget.displayData[bi];
		local down = DetauntTarget.sortButtonData[index].direction ~= DetauntTarget.BUTTON_DOWN;
		local result;
		local av = a[attr] or 0;
		local bv = b[attr] or 0;
		if down then
		    result = av > bv;
		else
		    result = av < bv;
		end
		return result;
	end
	return sortfunc;
end

local function getClassName(playerData)
	local className;
	if playerData.careerId == 0 and playerData.careerLine~=nil then
		className = GetCareerLine(playerData.careerLine, 1);
	else
		className = GetCareerName(playerData.careerId, 1 );
	end
	return className;
end

local function sortClassName( ai, bi)
	local a = DetauntTarget.displayData[ai];
	local b = DetauntTarget.displayData[bi];
	local aN = getClassName(a);
	local bN = getClassName(b);
	local result;
	local down = DetauntTarget.sortButtonData[3].direction ~= DetauntTarget.BUTTON_DOWN;
	if down then
	    result  = aN > bN;
	else
	    result  = aN < bN;
	end
	return result;
end

local function sortseen(ai,bi)
	local a = DetauntTarget.displayData[ai];
	local b = DetauntTarget.displayData[bi];
	local aV = a.year*10 + a.month*100 + a.day;
	local bV = b.year*10 + b.month*100 + b.day;
	local down = DetauntTarget.sortButtonData[5].direction ~= DetauntTarget.BUTTON_DOWN;
	local result;
	if down then
		result = aV > bV;
	else
		result = aV < bV;
	end
	return result;	
end

local function sortmonitor(ai,bi)
	local a = DetauntTarget.displayData[ai];
	local b = DetauntTarget.displayData[bi];
	local aName = DTHCommon.CleanName(a.name);
	local bName = DTHCommon.CleanName(b.name);
	local aV = 1;
	if VariableSettings.PermaWatched[aName]~=nil then
		aV = 0;
	end
	local bV = 1;
	if VariableSettings.PermaWatched[bName]~=nil then
		bV = 0;
	end
	local down = DetauntTarget.sortButtonData[6].direction ~= DetauntTarget.BUTTON_DOWN;
	if down then
		result = aV > bV;
	else
		result = aV < bV;
	end
	return result;	
end

DetauntTarget.currentSort = 2;
DetauntTarget.sortButtonData[1] = { active = false , direction = DetauntTarget.BUTTON_DOWN, sort = sorter(1) };
DetauntTarget.sortButtonData[2] = { active = true , direction = DetauntTarget.BUTTON_DOWN, sort = sorter(2) };
DetauntTarget.sortButtonData[3] = { active = false , direction = DetauntTarget.BUTTON_DOWN, sort = sortClassName };
DetauntTarget.sortButtonData[4] = { active = false , direction = DetauntTarget.BUTTON_DOWN, sort = sorter(4)};
DetauntTarget.sortButtonData[5] = { active = false , direction = DetauntTarget.BUTTON_DOWN, sort = sortseen };
DetauntTarget.sortButtonData[6] = { active = false , direction = DetauntTarget.BUTTON_DOWN, sort = sortmonitor };

function DetauntTarget.UpdateTargetData()
	local data = DetauntHelper.TargetData;
	local lData = {}
	local lSort = {}
	local i = 1;
	local filter;
	local v,filtered;
	local hasfilters = #DetauntTarget.Filters ~= 0;
	local record_count=0;
	for k,v in pairs(data) do
		record_count = record_count + 1;
		if  hasfilters then
			filtered = false;
			for _,filter in ipairs(DetauntTarget.Filters) do
				if not filter(v) then
					filtered = true;
					break;
				end
			end
			if filtered then
				continue;
			end
			lData[i] = v;
			lSort[i]=i;
			i = 1+i;
		end
	end
	DetauntTarget.displayData = lData;
	DetauntTarget.sortData = lSort;
	ListBoxSetDataTable(_TargetwindowID.."List", "DetauntTarget.displayData")
	DetauntTarget.RefreshList()
	DataUtils.SetListRowAlternatingTints( _windowID.."BaseWindow1List", MAX_VISIBLE_ROWS )
end

function DetauntTarget.UpdatePlayerIconAndClassName(rowFrame, playerData)
	local icon,dimX,dimY,className;
	if playerData.careerId == 0 and playerData.careerLine~=nil then
		icon,dimX,dimY = DetauntTargetInfo.GetCareerIconDataFromCareerLine(playerData.careerLine);
		className = GetCareerLine(playerData.careerLine, 1);
	else
		icon,dimX,dimY = DetauntTargetInfo.GetCareerIconDataFromCareerID(playerData.careerId);
		className = GetCareerName(playerData.careerId, 1 );
	end
	LabelSetText(rowFrame.."PlayerClass",className);
	local texture,tx,ty = GetIconData(icon);
	DynamicImageSetTexture( rowFrame .. "Icon", texture, tx, ty );
	DynamicImageSetTextureDimensions (rowFrame .. "Icon", dimX, dimY);
end

function DetauntTarget.PopulateListDisplayData()
	local PopulatorIndices = _G[_TargetwindowID.."List"].PopulatorIndices;
	local rowT = _TargetwindowID.."ListRow";
	if (PopulatorIndices ~= nil) then				
		for rowIndex, dataIndex in ipairs (PopulatorIndices) do
			local playerData = DetauntTarget.displayData[ dataIndex ];
			local rowFrame = rowT..rowIndex;
			DetauntTarget.UpdatePlayerIconAndClassName(rowFrame, playerData);
			LabelSetText(rowFrame.."PlayerRank",StringToWString(""..playerData.rank));
			local mind,avgd,maxd = DetauntTargetInfo.GetDPS(playerData);
			local dpsStr = string.format("%04d",avgd);
			LabelSetText(rowFrame.."PlayerDPS",StringToWString(dpsStr));
			local last = string.format("%02d/%02d/%d",playerData.month,playerData.day,playerData.year);
			LabelSetText(rowFrame.."PlayerLast",StringToWString(last));
			local sName = DTHCommon.CleanName(playerData.name);
			local texture,tx,ty;
			if VariableSettings.PermaWatched[sName]~=nil then
				texture,tx,ty = GetIconData(108);
			else
				texture,tx,ty = GetIconData(160);
			end
			DynamicImageSetTexture( rowFrame .. "Eye", texture, tx, ty );
			DynamicImageSetTextureDimensions (rowFrame .. "Eye", 24, 24);
			if DetauntTargetInfo.IsInScenario(playerData) then				
				LabelSetTextColor(rowFrame.."PlayerName", 0, 255, 0);
			else
				LabelSetTextColor(rowFrame.."PlayerName", 255, 204, 102);
			end
		end
	end      
end

function DetauntTarget.OnEyeLButtonUp()
	local activeWindow = SystemData.ActiveWindow.name;
	local PopulatorIndices = _G[_TargetwindowID.."List"].PopulatorIndices;
	local match_str = _TargetwindowID.."ListRow(.+)Eye";
	local index = tonumber( activeWindow:match(match_str) );
	local dataIndex = PopulatorIndices[index];
	local playerData = DetauntTarget.displayData[ dataIndex ];
	local sName = DTHCommon.CleanName(playerData.name);
	local texture,tx,ty;
	if VariableSettings.PermaWatched[sName]~=nil then
		VariableSettings.PermaWatched[sName]=nil;
		texture,tx,ty = GetIconData(160);
	else
		VariableSettings.PermaWatched[sName]=DetauntHelperMonitor.GetAnchorData(  );
		texture,tx,ty = GetIconData(108);
	end
	DynamicImageSetTexture( activeWindow, texture, tx, ty );
	DynamicImageSetTextureDimensions (activeWindow , 24, 24);
end

function DetauntTarget.OnDeleteTarget()
	local activeWindow = SystemData.ActiveWindow.name;
	local PopulatorIndices = _G[_TargetwindowID.."List"].PopulatorIndices;
	local match_str = _TargetwindowID.."ListRow(.+)Delete";
	local index = tonumber( activeWindow:match(match_str) );
	local dataIndex = PopulatorIndices[index];
	local wname = DetauntTarget.displayData[ dataIndex ].name;	
	local sName = DTHCommon.CleanName(wname);
	DetauntHelper.TargetData[sName] = nil;
	DetauntTarget.UpdateTargetData();
end



function DetauntTarget.UpdateListButtonStates()
    -- sort button arrows
    for index, data in pairs(DetauntTarget.sortButtonData)
    do
        local buttonName = _TargetwindowID.."SortButton"..index;        
        if (data.active == true)
        then
            WindowSetShowing(buttonName.."DownArrow", (data.direction == DetauntTarget.BUTTON_DOWN) )
            WindowSetShowing(buttonName.."UpArrow",   (data.direction == DetauntTarget.BUTTON_UP))
        else
            WindowSetShowing(buttonName.."DownArrow", false)
            WindowSetShowing(buttonName.."UpArrow",   false)
        end
    end
end

function DetauntTarget.RefreshList()
    -- set up the display ordering and filter settings
    local displayOrder = {}    
    if (DetauntTarget.displayData == nil) then
        return
    end
    local sortfunc = DetauntTarget.sortButtonData[DetauntTarget.currentSort].sort;
    table.sort(DetauntTarget.sortData, sortfunc)
    ListBoxSetDisplayOrder(_TargetwindowID.."List", DetauntTarget.sortData)   
end

function DetauntTarget.ChangeSorting()
    -- Update the sort buttons
    local buttonIndex = WindowGetId( SystemData.ActiveWindow.name );
    for index, data in pairs(DetauntTarget.sortButtonData) do
        if (index == buttonIndex) then
            DetauntTarget.currentSort = index
            if (data.active == true) then
                data.direction = not data.direction
            else
                data.active = true
            end
        else
            data.active = false
        end
    end
    DetauntTarget.UpdateListButtonStates()
    DetauntTarget.RefreshList()
end

function DetauntTarget.OnMouseOverSortButton()
end

function DetauntTarget.OnReset()
	DetauntTarget.Filters = {};
	local _blank = L"";
	TextEditBoxSetText( _TargetwindowID .. "LevelRangeFrom", _blank );
	TextEditBoxSetText( _TargetwindowID .. "LevelRangeTo", _blank );
	TextEditBoxSetText( _TargetwindowID .. "DPSRangeFrom", _blank );
	TextEditBoxSetText( _TargetwindowID .. "DPSRangeTo", _blank );
	TextEditBoxSetText( _TargetwindowID .. "SeenRangeFrom", _blank );
	TextEditBoxSetText( _TargetwindowID .. "SeenRangeTo", _blank );
	TextEditBoxSetText( _TargetwindowID .. "NameValue", _blank );
	TextEditBoxSetText( _TargetwindowID .. "ClassValue", _blank );
	ButtonSetPressedFlag( _TargetwindowID .. "MonitorOnButton", false);
	ButtonSetPressedFlag( _TargetwindowID .. "MonitorOffButton", false);	
end

local function stringMatcher(attr,str_val)
	str_val = WStringToString(str_val);
	local function filter(playerData)
		local p_val = WStringToString(playerData[attr]);
		return (p_val:match(str_val) ~=nill );
	end
	return filter;
end

local function classMatcher(str_val)
	str_val = WStringToString(str_val);
	local function filter(playerData)
		local cName = WStringToString(getClassName(playerData));
		return (cName:match(str_val) ~=nill );
	end
	return filter;
end

local function valueMatcher(attr,val,isless)
	local function filter(playerData)
		local local_val = playerData[attr];
		local result;
		if local_val==nil then
			local_val=0;
		end
		if isless then
			result = local_val<=val;
		else
			result =  local_val>=val;
		end
		return result;
	end
	return filter;
end


local function dateMatcher(val,isless)
	local function filter(playerData)
		local aV = playerData.year*10 + playerData.month*100 + playerData.day;
		if isless then
			return aV<=val;
		else
			return aV>=val;
		end
	end
	return filter;
end

local function text2Date(txt)
	txt = WStringToString(txt);
	local m,d,y = string.match(txt,"(%d+)/(%d+)/(%d+)");
	if m==nil then
		return nil;
	end
	m = tonumber(m);
	d = tonumber(d);
	y = tonumber(y);
	if m<=0 or m>12 then
		return nil;
	end
	if d<=0 or d>31 then
		return nil;
	end
	if y<1000 or y>9999 then
		return nil;
	end
	return (y*10 + m*100 + d);
end

local function isWatched(playerData)
	local sName = DTHCommon.CleanName(playerData.name);
	if VariableSettings.PermaWatched[sName]~=nil then
		return true;
	else
		return false;
	end
end

local function watchMatch( ison )
	local function filter(playerData)
		return isWatched(playerData)==ison;
	end
	return filter;
end

function DetauntTarget.OnApply()
	DetauntTarget.Filters = {};
	local _blank = L"";
	local nval;
	local fval = TextEditBoxGetText( _TargetwindowID .. "LevelRangeFrom");
	if _blank~=fval then
		nval = tonumber(fval);
		if nval~=nil then
			DetauntTarget.Filters[1+#DetauntTarget.Filters] = valueMatcher("rank",nval,false);
		end
	end
	fval = TextEditBoxGetText( _TargetwindowID .. "LevelRangeTo");
	if _blank~=fval then
		nval = tonumber(fval);
		if nval~=nil then
			DetauntTarget.Filters[1+#DetauntTarget.Filters] = valueMatcher("rank",nval,true);
		end
	end
	fval = TextEditBoxGetText( _TargetwindowID .. "DPSRangeFrom");
	if _blank~=fval then
		nval = tonumber(fval);
		if nval~=nil then
			DetauntTarget.Filters[1+#DetauntTarget.Filters] = valueMatcher("avg_dps",nval,false);
		end
	end
	fval = TextEditBoxGetText( _TargetwindowID .. "DPSRangeTo");
	if _blank~=fval then
		nval = tonumber(fval);
		if nval~=nil then
			DetauntTarget.Filters[1+#DetauntTarget.Filters] = valueMatcher("avg_dps",nval,true);
		end
	end
	fval = text2Date(TextEditBoxGetText( _TargetwindowID .. "SeenRangeFrom"));
	if fval~=nil then
		DetauntTarget.Filters[1+#DetauntTarget.Filters] = dateMatcher(fval,false);
	end
	fval = text2Date(TextEditBoxGetText( _TargetwindowID .. "SeenRangeTo"));
	if fval~=nil then
		DetauntTarget.Filters[1+#DetauntTarget.Filters] = dateMatcher(fval,true);
	end
	fval = TextEditBoxGetText( _TargetwindowID .. "NameValue");
	if _blank~=fval then
		DetauntTarget.Filters[1+#DetauntTarget.Filters] = stringMatcher("name",fval);
	end
	fval = TextEditBoxGetText( _TargetwindowID .. "ClassValue");
	if _blank~=fval then
		DetauntTarget.Filters[1+#DetauntTarget.Filters] = classMatcher(fval);
	end
	if ButtonGetPressedFlag( _TargetwindowID .. "MonitorOnButton") then
		DetauntTarget.Filters[1+#DetauntTarget.Filters] = watchMatch(true);
	elseif	ButtonGetPressedFlag( _TargetwindowID .. "MonitorOffButton") then
		DetauntTarget.Filters[1+#DetauntTarget.Filters] = watchMatch(false);
	end
	if DetauntHelperConfig.InScenario and ButtonGetPressedFlag(_TargetwindowID .. "ScenarioButton") then
		DetauntTarget.Filters[1+#DetauntTarget.Filters] = DetauntTargetInfo.IsInScenario;
	end
	DetauntTarget.UpdateTargetData();
end

function DetauntTarget.OnBarPropertyButton()
	local partial = _TargetwindowID .. "Monitor";
	local activeWindow = SystemData.ActiveWindow.name;
	local buttonName = activeWindow.."Button";
	local setting = SystemData.ActiveWindow.name:match(partial.."(.*)");
	local other = "On";
	if setting == "On" then
		other = "Off";
	end
	ButtonSetPressedFlag(partial..other.."Button",false);
	local checked = not ButtonGetPressedFlag(buttonName);
	ButtonSetPressedFlag( buttonName, checked );	
end

function DetauntTarget.OnScenarioButton()
	if not ButtonGetDisabledFlag(_TargetwindowID .. "ScenarioButton") then
		local activeWindow = SystemData.ActiveWindow.name;
		local buttonName = activeWindow.."Button";
		local checked = not ButtonGetPressedFlag(buttonName);
		ButtonSetPressedFlag( buttonName, checked );
	end
end

function DetauntTarget.OnRightClickBar()
	local activeWindow = SystemData.ActiveWindow.name;
	local PopulatorIndices = _G[_TargetwindowID.."List"].PopulatorIndices;
	local match_str = _TargetwindowID.."ListRow(.+)";
	local index = tonumber( activeWindow:match(match_str) );
	local dataIndex = PopulatorIndices[index];
	local wname = DetauntTarget.displayData[ dataIndex ].name;	
	local sName = DTHCommon.CleanName(wname);
	DetauntHelperMonitor.CreateWindow(sName);
end