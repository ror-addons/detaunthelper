local _windowID = "DTHConfigWindow";
local _TargetwindowID = _windowID.."BaseWindow1";
local VariableSettings;
DetauntHelperConfig = {};
local function DoTabSelect(tab_num)
	local i
	for i=1,3,1 do
		local tab = _windowID.."TabsTab"..i;
		local setit = true;
		--local x=770 y=300
		if i~= tab_num then
			setit = false;
			x = 0
			y = 0
		end
		--WindowSetDimensions( _windowID.."BaseWindow"..i, x, y);
		WindowSetShowing( _windowID.."BaseWindow"..i, setit);
		ButtonSetPressedFlag( tab, setit );
		ButtonSetStayDownFlag( tab, setit );
	end
end

DetauntHelperConfig.InScenario = false;

local DetauntHelperConfigTime = 0;

function DetauntHelperConfig.UpdateTime(elapsed)
	DetauntHelperConfigTime = DetauntHelperConfigTime + elapsed;
end

function DetauntHelperConfig.GetTime()
	return DetauntHelperConfigTime;
end


DetauntHelperConfig.Fonts = {'font_chat_text','font_clear_tiny','font_clear_small','font_clear_medium','font_clear_large',
				   'font_clear_small_bold','font_clear_medium_bold','font_clear_large_bold','font_default_text_small',
				   'font_default_text','font_default_text_large','font_default_text_huge','font_journal_text'};

--Default settings
local DHDefaults = {
    DHAnchor = { "center", "center", "Root", 0, 0 },
    TargCount = 5,
    ResetOnDetaunt = true,
    Threshold = 10,
    MonitorOn = 4,
    ClearOn = 3,
    TargetOn = 2,
    BarSize = 0,
    Font = 1,
    FontColor = 2,
    DataVersion = 0
};


function DetauntHelperConfig.UpdateDataVersion()
	if VariableSettings["DataVersion"]==nil then
		DTHCommon.CopyAtoBIfNil(DHDefaults,VariableSettings);
	end
	if VariableSettings["DataVersion"]==0 then
		local BarSettings = {};
		BarSettings["Font"] = 8;
		BarSettings["FontColor"] = 2;
		BarSettings["BarProperties"] = { true, true, true };--icon,label,dps
		BarSettings["HeightBar"] = 1;
		BarSettings["WidthBar"] = 1;
		BarSettings["ScaleBar"] = 1;
		BarSettings["LabelDimensions"] = {187.1428527832,24};
		BarSettings["DamageDimensions"] = {64.857147216797,24};
		BarSettings["WindowDimensions"] = {320,48};
		VariableSettings["BarSettings"] = BarSettings;
		VariableSettings["DataVersion"]=1;
	end
	if VariableSettings["DataVersion"]==1 then
		VariableSettings["TargetAcquiredSound"] = 214;
		VariableSettings["DetauntReadySound"] = 218;
		VariableSettings["DataVersion"]=2;
	end
	if VariableSettings["DataVersion"]==2 then
		VariableSettings["MaximumRecords"] = 1000;
		VariableSettings["DataVersion"]=3;
	end
	if VariableSettings["DataVersion"]==3 then
		VariableSettings["SoundOn"] = true;
		VariableSettings["DataVersion"]=4;
	end	
end

function DetauntHelperConfig.GetCurrentConfig()
	return VariableSettings;
end

function DetauntHelperConfig.Initialize()
	if DetauntHelper.DetauntHelperSettings==nil then
		DetauntHelper.DetauntHelperSettings = {};
	end
	VariableSettings = DetauntHelper.DetauntHelperSettings;
	if VariableSettings.PermaWatched == nil then
		VariableSettings.PermaWatched = {}
	end
	DetauntHelperConfig.UpdateDataVersion();
	DetauntHelper.AddCommand("menu",DHLang.GetString(DHStrings.DH_MENU),DetauntHelperConfig.UpdateConfig);
	--Setup Main Window
	CreateWindow( _windowID , false);
	LabelSetText( _windowID.."BaseTitleBarText", DHLang.GetString(DHStrings.TITLE) );
	WindowSetShowing( _windowID , false);
	--Register For Events
	RegisterEventHandler( SystemData.Events.SCENARIO_BEGIN, "DetauntHelperConfig.OnScenarioBegin");
	RegisterEventHandler( SystemData.Events.SCENARIO_END, "DetauntHelperConfig.OnScenarioEnd" );
	DetauntBarSettings.InitUI();
	DetauntTarget.InitUI();
	DetauntAbilityManager.InitUI();
	return VariableSettings;
end

function DetauntHelperConfig.OnScenarioBegin()
	ButtonSetDisabledFlag(_TargetwindowID .. "ScenarioButton", false);
	DetauntHelperConfig.InScenario = true;
end

function DetauntHelperConfig.OnScenarioEnd()
	ButtonSetDisabledFlag(_TargetwindowID .. "ScenarioButton", true);
	ButtonSetPressedFlag( _TargetwindowID .. "ScenarioButton", false );
	DetauntHelperConfig.InScenario = false;
end

function DetauntHelperConfig.UpdateConfig()
	WindowSetShowing( _windowID , not WindowGetShowing( _windowID ));
	if ( WindowGetShowing( _windowID, true ) ) then
		DetauntBarSettings.ShowUI();
		DetauntTarget.ShowUI();
		DetauntAbilityManager.ShowUI();
		WindowSetAlpha( _windowID.."BaseBackgroundBackground" , 1 );
		DoTabSelect(2);
	else
		DetauntBarSettings.Save();
	end
	return true;
end

function DetauntHelperConfig.OnTabSelect1()
	DetauntTarget.UpdateTargetData();
	DetauntTarget.UpdateListButtonStates();
	DoTabSelect(1);
end

function DetauntHelperConfig.OnTabSelect3()
	DoTabSelect(3);
end

function DetauntHelperConfig.OnTabSelect2()
	DoTabSelect(2);
end