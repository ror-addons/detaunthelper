DHStrings = {};
DHStrings.TITLE                     = 1;
DHStrings.CLICK_BEHAVIOR            = 2;
DHStrings.CLICK_TARGET_ON           = 3;
DHStrings.CLICK_CLEAR_ON            = 4;
DHStrings.CLICK_MONITOR_ON          = 5;
DHStrings.MISC                      = 6;
DHStrings.BAR_SIZE                  = 7;
DHStrings.BAR_QTY                   = 8;
DHStrings.EXP_DELAY                 = 9;
DHStrings.UNKNOWN                   = 10;
DHStrings.DO_RESET                  = 11;
DHStrings.FONT_STYLE                = 12;
DHStrings.FONT_TYPE                 = 13;
DHStrings.FONT_COLOR                = 14;
DHStrings.FONT_CHAT_TEXT            = 15;
DHStrings.FONT_CLEAR_TINY           = 16;
DHStrings.FONT_CLEAR_SMALL          = 17;
DHStrings.FONT_CLEAR_MEDIUM         = 18;
DHStrings.FONT_CLEAR_LARGE          = 19;
DHStrings.FONT_CLEAR_SMALL_BOLD     = 20;
DHStrings.FONT_CLEAR_MEDIUM_BOLD    = 21;
DHStrings.FONT_CLEAR_LARGE_BOLD     = 22;
DHStrings.FONT_DEFAULT_TEXT_SMALL   = 23;
DHStrings.FONT_DEFAULT_TEXT         = 24;
DHStrings.FONT_DEFAULT_TEXT_LARGE   = 25;
DHStrings.FONT_DEFAULT_TEXT_HUGE    = 26;
DHStrings.FONT_JOURNAL_TEXT         = 27;
DHStrings.COLOR_BLACK               = 28;
DHStrings.COLOR_WHITE               = 29;
DHStrings.ITEMS_SHOWN               = 30;
DHStrings.ITEMS_SHOWN_ICON          = 31;
DHStrings.ITEMS_SHOWN_TARG          = 32;
DHStrings.ITEMS_SHOWN_DPS           = 33;
DHStrings.NAME_WIDTH                = 34;
DHStrings.BAR_HEIGHT                = 35;
DHStrings.BAR_SCALE                 = 36;
DHStrings.SOUND_NOTICE              = 37;
DHStrings.SOUND_TARG                = 38;
DHStrings.SOUND_RDY                 = 39;
DHStrings.MOUSE_DISABLED            = 40;
DHStrings.MOUSE_LEFT                = 41;
DHStrings.MOUSE_MIDDLE              = 42;
DHStrings.MOUSE_RIGHT               = 43;
DHStrings.LABEL_RANK                = 44;
DHStrings.LABEL_DPS                 = 45;
DHStrings.TAB_TARGETS               = 46;
DHStrings.TAB_WATCHES               = 47;
DHStrings.TAB_DISPLAY               = 48;
DHStrings.DH_COMMANDS               = 49;
DHStrings.DH_TARGTOP                = 50;
DHStrings.DH_TARGTOP_EX             = 51;
DHStrings.DH_RESET                  = 52;
DHStrings.DH_RESET_EX               = 53;
DHStrings.DH_MENU                   = 54;
DHStrings.DH_INITIALIZED            = 55;
DHStrings.DH_CMD_LIST               = 56;
DHStrings.DH_NERFED                 = 57;
DHStrings.NAME                      = 58;
DHStrings.CLASS                     = 59;
DHStrings.LAST_SEEN                 = 60;
DHStrings.MONITOR                   = 61;
--Target Stuff
DHStrings.ACTIVE                    = 62;
DHStrings.INACTIVE                  = 63;
DHStrings.REC_QTY                   = 64;
DHStrings.RECS_CLEANED              = 65;
DHStrings.RECS_IN_DB                = 66;
DHStrings.TARGET_MSG                = 67;
DHStrings.LAYOUT_ICON               = 68;
DHStrings.LAYOUT_ANCHOR             = 69;
DHStrings.LAYOUT_MONITOR            = 70;
DHStrings.SCENARIO                  = 71;
DHStrings.ABILITY                   = 72;
DHStrings.PACK_DB                   = 73;
DHStrings.SOUND_ENABLED             = 74;


local DHStringMapping = {};
dh_english_mapping = {};
dh_english_mapping[DHStrings.UNKNOWN]                   = StringToWString("UNKNOWN");
dh_english_mapping[DHStrings.TITLE]                     = StringToWString("Detaunt Helper Settings");
dh_english_mapping[DHStrings.CLICK_BEHAVIOR]            = StringToWString("Click Behavior");
dh_english_mapping[DHStrings.CLICK_TARGET_ON]           = StringToWString("Target On");
dh_english_mapping[DHStrings.CLICK_CLEAR_ON]            = StringToWString("Clear On");
dh_english_mapping[DHStrings.CLICK_MONITOR_ON]          = StringToWString("Monitor On");
dh_english_mapping[DHStrings.MISC]                      = StringToWString("Miscellaneous");
dh_english_mapping[DHStrings.BAR_SIZE]                  = StringToWString("Bar Size");
dh_english_mapping[DHStrings.BAR_QTY]                   = StringToWString("Number of Bars to Display");
dh_english_mapping[DHStrings.EXP_DELAY]                 = StringToWString("Seconds to Delay Expiration");
dh_english_mapping[DHStrings.DO_RESET]                  = StringToWString("Reset on Detaunt");
dh_english_mapping[DHStrings.FONT_STYLE]                = StringToWString("Bar Font Styles");
dh_english_mapping[DHStrings.FONT_TYPE]                 = StringToWString("Font Type");
dh_english_mapping[DHStrings.FONT_COLOR]                = StringToWString("Font Color");
--Font Stuff
dh_english_mapping[DHStrings.FONT_CHAT_TEXT]            = StringToWString("Font Chat Text");
dh_english_mapping[DHStrings.FONT_CLEAR_TINY]           = StringToWString("Font Clear Tiny");
dh_english_mapping[DHStrings.FONT_CLEAR_SMALL]          = StringToWString("Font Clear Small");
dh_english_mapping[DHStrings.FONT_CLEAR_MEDIUM]         = StringToWString("Font Clear Medium");
dh_english_mapping[DHStrings.FONT_CLEAR_LARGE]          = StringToWString("Font Clear Large");
dh_english_mapping[DHStrings.FONT_CLEAR_SMALL_BOLD]     = StringToWString("Font Clear Small Bold");
dh_english_mapping[DHStrings.FONT_CLEAR_MEDIUM_BOLD]    = StringToWString("Font Clear Medium Bold");
dh_english_mapping[DHStrings.FONT_CLEAR_LARGE_BOLD]     = StringToWString("Font Clear Large Bold");
dh_english_mapping[DHStrings.FONT_DEFAULT_TEXT_SMALL]   = StringToWString("Font Default Text Small");
dh_english_mapping[DHStrings.FONT_DEFAULT_TEXT]         = StringToWString("Font Default Text");
dh_english_mapping[DHStrings.FONT_DEFAULT_TEXT_LARGE]   = StringToWString("Font Default Text Large");
dh_english_mapping[DHStrings.FONT_DEFAULT_TEXT_HUGE]    = StringToWString("Font Default Text Huge");
dh_english_mapping[DHStrings.FONT_JOURNAL_TEXT]         = StringToWString("Font Journal Text");
dh_english_mapping[DHStrings.COLOR_BLACK]               = StringToWString("Black");
dh_english_mapping[DHStrings.COLOR_WHITE]               = StringToWString("White");
dh_english_mapping[DHStrings.ITEMS_SHOWN]               = StringToWString("Bar Items Shown");
dh_english_mapping[DHStrings.ITEMS_SHOWN_ICON]          = StringToWString("Icon");
dh_english_mapping[DHStrings.ITEMS_SHOWN_TARG]          = StringToWString("Target");
dh_english_mapping[DHStrings.ITEMS_SHOWN_DPS]           = StringToWString("DPS");
dh_english_mapping[DHStrings.NAME_WIDTH]                = StringToWString("Name Width");
dh_english_mapping[DHStrings.BAR_HEIGHT]                = StringToWString("Bar Height");
dh_english_mapping[DHStrings.BAR_SCALE]                 = StringToWString("Scale");
dh_english_mapping[DHStrings.SOUND_NOTICE]              = StringToWString("Sound Notifications");
dh_english_mapping[DHStrings.SOUND_TARG]                = StringToWString("Targeted");
dh_english_mapping[DHStrings.SOUND_RDY]                 = StringToWString("Ready");
--Mouse Combo Settings
dh_english_mapping[DHStrings.MOUSE_DISABLED]            = StringToWString("Disabled");
dh_english_mapping[DHStrings.MOUSE_LEFT]                = StringToWString("Left");
dh_english_mapping[DHStrings.MOUSE_MIDDLE]              = StringToWString("Middle");
dh_english_mapping[DHStrings.MOUSE_RIGHT]               = StringToWString("Right");
dh_english_mapping[DHStrings.LABEL_RANK]                = StringToWString("Rank");
dh_english_mapping[DHStrings.LABEL_DPS]                 = StringToWString("DPS");
--Tabs
dh_english_mapping[DHStrings.TAB_TARGETS]               = StringToWString("Targets");
dh_english_mapping[DHStrings.TAB_WATCHES]               = StringToWString("Watches");
dh_english_mapping[DHStrings.TAB_DISPLAY]               = StringToWString("Display");
dh_english_mapping[DHStrings.DH_COMMANDS]               = StringToWString("Detaunt Helper Commands:");
dh_english_mapping[DHStrings.DH_TARGTOP]                = StringToWString("Target top dps player:");
dh_english_mapping[DHStrings.DH_TARGTOP_EX]             = StringToWString("/script DetauntHelper.TargetTop()");
dh_english_mapping[DHStrings.DH_RESET]                  = StringToWString("Reset list:");
dh_english_mapping[DHStrings.DH_RESET_EX]               = StringToWString("/script DetauntHelper.Reset()");
dh_english_mapping[DHStrings.DH_MENU]                   = StringToWString("Show the UI:");
dh_english_mapping[DHStrings.DH_INITIALIZED]            = StringToWString("[Detaunt Helper] Initialized");
dh_english_mapping[DHStrings.DH_CMD_LIST]               = StringToWString("[Detaunt Helper] Use /dh for a list of commands.");
dh_english_mapping[DHStrings.DH_NERFED]                 = StringToWString("[Detaunt Helper] Detected NerfButtons, adding custom check:dht");
dh_english_mapping[DHStrings.NAME]                      = StringToWString("Name");
dh_english_mapping[DHStrings.CLASS]                     = StringToWString("Class");
dh_english_mapping[DHStrings.LAST_SEEN]                 = StringToWString("Last Seen");
dh_english_mapping[DHStrings.MONITOR]                   = StringToWString("Monitor");
--Target Stuff
dh_english_mapping[DHStrings.ACTIVE]                    = StringToWString("On");
dh_english_mapping[DHStrings.INACTIVE]                  = StringToWString("Off");
dh_english_mapping[DHStrings.REC_QTY]                   = StringToWString("Maximum Records");
dh_english_mapping[DHStrings.RECS_CLEANED]              = StringToWString("[Detaunt Helper] Database Shrank by:");
dh_english_mapping[DHStrings.RECS_IN_DB]                = StringToWString("[Detaunt Helper] Database Entries:");
dh_english_mapping[DHStrings.TARGET_MSG]                = StringToWString("Detaunt Helper Target:");
dh_english_mapping[DHStrings.LAYOUT_ICON]               = StringToWString("Detaunt Helper Icon");
dh_english_mapping[DHStrings.LAYOUT_ANCHOR]             = StringToWString("Detaunt Helper Anchor");
dh_english_mapping[DHStrings.LAYOUT_MONITOR]            = StringToWString("Detaunt Helper Monitor Anchor");
dh_english_mapping[DHStrings.SCENARIO]                  = StringToWString("Scenario");
dh_english_mapping[DHStrings.ABILITY]                   = StringToWString("Abilities");
dh_english_mapping[DHStrings.PACK_DB]                   = StringToWString("Pack DB");
dh_english_mapping[DHStrings.SOUND_ENABLED]             = StringToWString("Sound Enabled");

DHStringMapping[SystemData.Settings.Language.ENGLISH]   = dh_english_mapping;

local DHBaseTable = {};
DHBaseTable.__index = dh_english_mapping;

local function DHGetLanguageMapping(lang)
    if DHStringMapping[lang] == nil then
        DHStringMapping[lang] = {};
        setmetatable(DHStringMapping[lang],DHBaseTable);
    end
    return DHStringMapping[lang];
end


local function DHGetActiveLanguageMapping()
    return DHGetLanguageMapping(SystemData.Settings.Language.active);
end

DHLang = {};

function DHLang.GetString(str_id)
    local mapping = DHGetActiveLanguageMapping();
    local str_val = mapping[str_id];
    if str_val == nil then
        str_val = mapping[DHStrings.UNKNOWN];
    end
    return str_val;
end

local dh_mouse_combo_settings;

function DHLang.GetMouseComboSettings()
    if dh_mouse_combo_settings == nil then
        dh_mouse_combo_settings = {};
        dh_mouse_combo_settings[1] = DHLang.GetString(DHStrings.MOUSE_DISABLED);
        dh_mouse_combo_settings[2] = DHLang.GetString(DHStrings.MOUSE_LEFT);
        dh_mouse_combo_settings[3] = DHLang.GetString(DHStrings.MOUSE_MIDDLE);
        dh_mouse_combo_settings[4] = DHLang.GetString(DHStrings.MOUSE_RIGHT);
    end
    return dh_mouse_combo_settings;
end

local dh_font_colors;

function DHLang.GetFontColors()
    if dh_font_colors == nil then
        dh_font_colors = {};
        dh_font_colors[1] = DHLang.GetString(DHStrings.COLOR_BLACK);
        dh_font_colors[2] = DHLang.GetString(DHStrings.COLOR_WHITE);
    end
    return dh_font_colors;
end

local dh_font_list;

function DHLang.GetFontNames()
    if dh_font_list == nil then
        dh_font_list = {};
        dh_font_list[1] = DHLang.GetString(DHStrings.FONT_CHAT_TEXT);
        dh_font_list[2] = DHLang.GetString(DHStrings.FONT_CLEAR_TINY);
        dh_font_list[3] = DHLang.GetString(DHStrings.FONT_CLEAR_SMALL);
        dh_font_list[4] = DHLang.GetString(DHStrings.FONT_CLEAR_MEDIUM);
        dh_font_list[5] = DHLang.GetString(DHStrings.FONT_CLEAR_LARGE);
        dh_font_list[6] = DHLang.GetString(DHStrings.FONT_CLEAR_SMALL_BOLD);
        dh_font_list[7] = DHLang.GetString(DHStrings.FONT_CLEAR_MEDIUM_BOLD);
        dh_font_list[8] = DHLang.GetString(DHStrings.FONT_CLEAR_LARGE_BOLD);
        dh_font_list[9] = DHLang.GetString(DHStrings.FONT_DEFAULT_TEXT_SMALL);
        dh_font_list[10] = DHLang.GetString(DHStrings.FONT_DEFAULT_TEXT);
        dh_font_list[11] = DHLang.GetString(DHStrings.FONT_DEFAULT_TEXT_LARGE);
        dh_font_list[12] = DHLang.GetString(DHStrings.FONT_DEFAULT_TEXT_HUGE);
        dh_font_list[13] = DHLang.GetString(DHStrings.FONT_JOURNAL_TEXT);
    end
    return dh_font_list;
end


dh_french_mapping = DHGetLanguageMapping(SystemData.Settings.Language.FRENCH);
dh_french_mapping[DHStrings.UNKNOWN]                   = StringToWString("INCONNU");
dh_french_mapping[DHStrings.TITLE]                     = StringToWString("Detaunt Aide Param�tres");
dh_french_mapping[DHStrings.CLICK_BEHAVIOR]            = StringToWString("Cliquez Sur Le Comportement");
dh_french_mapping[DHStrings.CLICK_TARGET_ON]           = StringToWString("Cible");
dh_french_mapping[DHStrings.CLICK_CLEAR_ON]            = StringToWString("Remettre");
dh_french_mapping[DHStrings.CLICK_MONITOR_ON]          = StringToWString("Surveiller");
dh_french_mapping[DHStrings.MISC]                      = StringToWString("Vari�");
dh_french_mapping[DHStrings.BAR_SIZE]                  = StringToWString("Bar Taille");
dh_french_mapping[DHStrings.BAR_QTY]                   = StringToWString("Nombre de Barres � Afficher");
dh_french_mapping[DHStrings.EXP_DELAY]                 = StringToWString("Secondes de D�lai d'Expiration");
dh_french_mapping[DHStrings.DO_RESET]                  = StringToWString("Reset sur Detaunt");
dh_french_mapping[DHStrings.FONT_STYLE]                = StringToWString("Style de Police de Barres");
dh_french_mapping[DHStrings.FONT_TYPE]                 = StringToWString("Police de Type");
dh_french_mapping[DHStrings.FONT_COLOR]                = StringToWString("Couleur de Police");
dh_french_mapping[DHStrings.COLOR_BLACK]               = StringToWString("Couleur Noire");
dh_french_mapping[DHStrings.COLOR_WHITE]               = StringToWString("Blanc");
dh_french_mapping[DHStrings.ITEMS_SHOWN]               = StringToWString("�l�ments Figurant");
dh_french_mapping[DHStrings.ITEMS_SHOWN_ICON]          = StringToWString("Ic�ne");
dh_french_mapping[DHStrings.ITEMS_SHOWN_TARG]          = StringToWString("Cible");
dh_french_mapping[DHStrings.ITEMS_SHOWN_DPS]           = StringToWString("DPS");
dh_french_mapping[DHStrings.NAME_WIDTH]                = StringToWString("Largeur de Nom");
dh_french_mapping[DHStrings.BAR_HEIGHT]                = StringToWString("Hauteur Bar");
dh_french_mapping[DHStrings.BAR_SCALE]                 = StringToWString("Graduation");
dh_french_mapping[DHStrings.SOUND_NOTICE]              = StringToWString("Notifications Sonores");
dh_french_mapping[DHStrings.SOUND_TARG]                = StringToWString("Cibl�es");
dh_french_mapping[DHStrings.SOUND_RDY]                 = StringToWString("Prompt");
--Mouse Combo Settings
dh_french_mapping[DHStrings.MOUSE_DISABLED]            = StringToWString("Mutil�");
dh_french_mapping[DHStrings.MOUSE_LEFT]                = StringToWString("Gauche");
dh_french_mapping[DHStrings.MOUSE_MIDDLE]              = StringToWString("Milieu");
dh_french_mapping[DHStrings.MOUSE_RIGHT]               = StringToWString("Droit");
dh_french_mapping[DHStrings.LABEL_RANK]                = StringToWString("Rang");
dh_french_mapping[DHStrings.LABEL_DPS]                 = StringToWString("DPS");
--Tabs
dh_french_mapping[DHStrings.TAB_TARGETS]               = StringToWString("Cibles");
dh_french_mapping[DHStrings.TAB_WATCHES]               = StringToWString("Regard�");
dh_french_mapping[DHStrings.TAB_DISPLAY]               = StringToWString("Affichage");
dh_french_mapping[DHStrings.DH_COMMANDS]               = StringToWString("Detaunt Aide Commandes:");
dh_french_mapping[DHStrings.DH_TARGTOP]                = StringToWString("Objectif de Meilleur Joueur:");
dh_french_mapping[DHStrings.DH_TARGTOP_EX]             = StringToWString("/script DetauntHelper.TargetTop()");
dh_french_mapping[DHStrings.DH_RESET]                  = StringToWString("R�initialiser la Liste:");
dh_french_mapping[DHStrings.DH_RESET_EX]               = StringToWString("/script DetauntHelper.Reset()");
dh_french_mapping[DHStrings.DH_MENU]                   = StringToWString("Afficher l'Interface:");
dh_french_mapping[DHStrings.DH_INITIALIZED]            = StringToWString("[Detaunt Aide] Initialis�");
dh_french_mapping[DHStrings.DH_CMD_LIST]               = StringToWString("[Detaunt Aide] Utiliser /dh pour une liste de commandes.");
dh_french_mapping[DHStrings.DH_NERFED]                 = StringToWString("[Detaunt Aide] Nerfedbuttons d�tect�, l'ajout d'un contr�le personnalis�:dht");
dh_french_mapping[DHStrings.NAME]                      = StringToWString("Nom");
dh_french_mapping[DHStrings.CLASS]                     = StringToWString("Profession");
dh_french_mapping[DHStrings.LAST_SEEN]                 = StringToWString("A vu en Dernier");
dh_french_mapping[DHStrings.MONITOR]                   = StringToWString("Surveiller");
dh_french_mapping[DHStrings.ACTIVE]                    = StringToWString("Permis");
dh_french_mapping[DHStrings.INACTIVE]                  = StringToWString("Mutil�");
dh_french_mapping[DHStrings.REC_QTY]                   = StringToWString("Quantit� Maximale de Documents");
dh_french_mapping[DHStrings.RECS_CLEANED]              = StringToWString("[Detaunt Aide] Base de Donn�es a Diminu� de:");
dh_french_mapping[DHStrings.RECS_IN_DB]                = StringToWString("[Detaunt Aide] Base de Donn�es:");
dh_french_mapping[DHStrings.TARGET_MSG]                = StringToWString("Detaunt Aide Cible:");
dh_french_mapping[DHStrings.LAYOUT_ICON]               = StringToWString("Detaunt Aide Icon");
dh_french_mapping[DHStrings.LAYOUT_ANCHOR]             = StringToWString("Detaunt Aide Anchor");
dh_french_mapping[DHStrings.LAYOUT_MONITOR]            = StringToWString("Detaunt Aide Surveiller Anchor");
dh_french_mapping[DHStrings.SCENARIO]                  = StringToWString("Sc�nario"); 
dh_french_mapping[DHStrings.ABILITY]                   = StringToWString("Capacit� Detaunt");

--German

dh_german_mapping = DHGetLanguageMapping(SystemData.Settings.Language.GERMAN);
dh_german_mapping[DHStrings.UNKNOWN]                   = StringToWString("UNBEKANNTE");
dh_german_mapping[DHStrings.TITLE]                     = StringToWString("Detaunt Helfer Einstellungen");
dh_german_mapping[DHStrings.CLICK_BEHAVIOR]            = StringToWString("Klicken Sie auf Verhalten");
dh_german_mapping[DHStrings.CLICK_TARGET_ON]           = StringToWString("Ziel");
dh_german_mapping[DHStrings.CLICK_CLEAR_ON]            = StringToWString("L�schen");
dh_german_mapping[DHStrings.CLICK_MONITOR_ON]          = StringToWString("Mith�ren");
dh_german_mapping[DHStrings.MISC]                      = StringToWString("Verschieden");
dh_german_mapping[DHStrings.BAR_SIZE]                  = StringToWString("Bar Gr��e");
dh_german_mapping[DHStrings.BAR_QTY]                   = StringToWString("Anzahl der Bars Anzeigen");
dh_german_mapping[DHStrings.EXP_DELAY]                 = StringToWString("Sekunden Verz�gerung Expiration");
dh_german_mapping[DHStrings.DO_RESET]                  = StringToWString("Reset nach Detaunt");
dh_german_mapping[DHStrings.FONT_STYLE]                = StringToWString("Bar Schriftstile");
dh_german_mapping[DHStrings.FONT_TYPE]                 = StringToWString("Schriftart");
dh_german_mapping[DHStrings.FONT_COLOR]                = StringToWString("Farbe der Schrift");
dh_german_mapping[DHStrings.COLOR_BLACK]               = StringToWString("Noir");
dh_german_mapping[DHStrings.COLOR_WHITE]               = StringToWString("Wei�");
dh_german_mapping[DHStrings.ITEMS_SHOWN]               = StringToWString("Bar Posten");
dh_german_mapping[DHStrings.ITEMS_SHOWN_ICON]          = StringToWString("Ikon");
dh_german_mapping[DHStrings.ITEMS_SHOWN_TARG]          = StringToWString("Ziel");
dh_german_mapping[DHStrings.ITEMS_SHOWN_DPS]           = StringToWString("DPS");
dh_german_mapping[DHStrings.NAME_WIDTH]                = StringToWString("Breite des Namens");
dh_german_mapping[DHStrings.BAR_HEIGHT]                = StringToWString("H�he der Bar");
dh_german_mapping[DHStrings.BAR_SCALE]                 = StringToWString("Schuppe");
dh_german_mapping[DHStrings.SOUND_NOTICE]              = StringToWString("Sound-Meldungen");
dh_german_mapping[DHStrings.SOUND_TARG]                = StringToWString("Gezielt");
dh_german_mapping[DHStrings.SOUND_RDY]                 = StringToWString("Fertig");
--Mouse Combo Settings
dh_german_mapping[DHStrings.MOUSE_DISABLED]            = StringToWString("Deaktiviert");
dh_german_mapping[DHStrings.MOUSE_LEFT]                = StringToWString("Linke");
dh_german_mapping[DHStrings.MOUSE_MIDDLE]              = StringToWString("Mitte");
dh_german_mapping[DHStrings.MOUSE_RIGHT]               = StringToWString("Rechts");
dh_german_mapping[DHStrings.LABEL_RANK]                = StringToWString("Rang");
dh_german_mapping[DHStrings.LABEL_DPS]                 = StringToWString("DPS");
--Tabs
dh_german_mapping[DHStrings.TAB_TARGETS]               = StringToWString("Ziele");
dh_german_mapping[DHStrings.TAB_WATCHES]               = StringToWString("Beobachtet");
dh_german_mapping[DHStrings.TAB_DISPLAY]               = StringToWString("Bildschirm");
dh_german_mapping[DHStrings.DH_COMMANDS]               = StringToWString("Befehle der Detaunt Helfer:");
dh_german_mapping[DHStrings.DH_TARGTOP]                = StringToWString("Ziel besten Spieler:");
dh_german_mapping[DHStrings.DH_TARGTOP_EX]             = StringToWString("/script DetauntHelper.TargetTop()");
dh_german_mapping[DHStrings.DH_RESET]                  = StringToWString("reset the list:");
dh_german_mapping[DHStrings.DH_RESET_EX]               = StringToWString("/script DetauntHelper.Reset()");
dh_german_mapping[DHStrings.DH_MENU]                   = StringToWString("Anzeige der Schnittstelle:");
dh_german_mapping[DHStrings.DH_INITIALIZED]            = StringToWString("[Detaunt Helfer] Initialisiert");
dh_german_mapping[DHStrings.DH_CMD_LIST]               = StringToWString("[Detaunt Helfer] Einsatz /dh f�r eine Liste von Befehlen.");
dh_german_mapping[DHStrings.DH_NERFED]                 = StringToWString("[Detaunt Helfer] Erkannt Nerfedbuttons, Eine Eigene Anreise:dht");
dh_german_mapping[DHStrings.NAME]                      = StringToWString("Namens");
dh_german_mapping[DHStrings.CLASS]                     = StringToWString("Einstufung");
dh_german_mapping[DHStrings.LAST_SEEN]                 = StringToWString("Sah Letzten");
dh_german_mapping[DHStrings.MONITOR]                   = StringToWString("Mith�ren");
dh_german_mapping[DHStrings.ACTIVE]                    = StringToWString("On");
dh_german_mapping[DHStrings.INACTIVE]                  = StringToWString("Deaktiviert");
dh_german_mapping[DHStrings.REC_QTY]                   = StringToWString("H�chstbetrag der Eintr�ge");
dh_german_mapping[DHStrings.RECS_CLEANED]              = StringToWString("[Detaunt Helfer] Die Aufzeichnungen um:");
dh_german_mapping[DHStrings.RECS_IN_DB]                = StringToWString("[Detaunt Helfer] Anzahl der Eintr�ge:");
dh_german_mapping[DHStrings.TARGET_MSG]                = StringToWString("Detaunt Helfer Ziel:");
dh_german_mapping[DHStrings.LAYOUT_ICON]               = StringToWString("Detaunt Helfer Ikon");
dh_german_mapping[DHStrings.LAYOUT_ANCHOR]             = StringToWString("Detaunt Helfer Anchor");
dh_german_mapping[DHStrings.LAYOUT_MONITOR]            = StringToWString("Detaunt Helfer Mith�ren Anchor");
dh_german_mapping[DHStrings.SCENARIO]                  = StringToWString("Szenarium"); 
dh_german_mapping[DHStrings.ABILITY]                   = StringToWString("F�higkeit");