DetauntHelperNerf = {}

local _SequenceTypes= {};
_SequenceTypes["n"]    = 0;
_SequenceTypes["s"]    = 4;
_SequenceTypes["c"]    = 8;
_SequenceTypes["a"]    = 32;
_SequenceTypes["sc"]   = 12;
_SequenceTypes["sa"]   = 36;
_SequenceTypes["ac"]   = 40;
_SequenceTypes["sac"]  = 44;

local _ReverseSequenceTypes = {};
local kk,vv;
for kk,vv in pairs(_SequenceTypes) do
    _ReverseSequenceTypes[vv]=kk;
end

local _init = true;
local _nerf = false;
local DTHNHS;


local function DHListProvider()
    local key,val;
    local result = {};
    for key,val in pairs(_SequenceTypes) do
        table.insert(result,StringToWString(key));
    end
    return result;
end

local function DHDeleter( seq_name )
    local flag = _SequenceTypes[tostring(seq_name)];
    if  flag~=nil then
        DetauntHelperNerf.NerfDel(flag);
        return true
    end
    return false;
end

local function DHGetter( seq_name )
    local flag = _SequenceTypes[tostring(seq_name)];
    if flag~=nil then
        local binding = DTHNHS[flag];
        if binding~=nil then
            local _sequence = NerfedMemory.GetBinding(binding);
            if _sequence then
                return _sequence;
            end
        end
    end
    return {};
end

local function DHSetter( seq_name, seq )
    local flag = _SequenceTypes[tostring(seq_name)];
    if flag~=nil then
        local binding = DetauntHelperNerf.GetBindingKey(flag)
        NerfedMemory.SetBinding(binding, seq)
        DTHNHS[flag]=binding;
        return true;
    end
    return false;
end
local _registerd_nbsb = false;
function DetauntHelperNerf.Initialize()
    if _init and NerfedDecisions~=nil and LibSlash~=nill then
        _init = false;
        local cfg = DetauntHelperConfig.GetCurrentConfig();
        if cfg.DTHNS == nil then
            cfg.DTHNS = {};
        end
        DTHNHS = cfg.DTHNS;
        _nerf = true;
        EA_ChatWindow.Print( DHLang.GetString(DHStrings.DH_NERFED) );
        NerfedChecks.RegisterCheck("Detaunt Helper Target", function(abilityId, params) return DetauntHelperNerf.IsTargetDetauntTarget(abilityId, params); end, {'need'}, "dht")
        DetauntHelper.AddCommand("add",L"/dh add [n|s|c|a|sc|sa|ac|sac] nerfedsequence",DetauntHelperNerf.HandleNerfAddCommand);
        DetauntHelper.AddCommand("edit",L"/dh edit [n|s|c|a|sc|sa|ac|sac]",DetauntHelperNerf.HandleNerfEditCommand);
        DetauntHelper.AddCommand("del",L"/dh del [n|s|c|a|sc|sa|ac|sac]",DetauntHelperNerf.HandleNerfDelCommand);
        DetauntHelper.AddCommand("list",L"/dh del [n|s|c|a|sc|sa|ac|sac]",DetauntHelperNerf.HandleNerfListCommand);
    end
    if NBSBRegister~=nil and NBSBRegister.RegisterSequenceProvider~=nil then
        if not _registerd_nbsb then
            _registerd_nbsb = true;
            NBSBRegister.RegisterSequenceProvider( "DetauntHelper", DHListProvider, DHDeleter, DHGetter, DHSetter);
        end
    end
end

--NerfedButtonsDetauntCustomCheck
function DetauntHelperNerf.IsTargetDetauntTarget(abilityId, params)
    local needDTHTarget = params["need"];
    local isDTHTarget = DetauntHelper.CurrentTargetName == DetauntHelper.DetauntTargetName;
    if needDTHTarget == "-" then
        isDTHTarget = not isDTHTarget;
    end
    return isDTHTarget;
end

function DetauntHelperNerf.GetAbilityForFlag(flag)
    local atype = GameData.PlayerActions.NONE;
    local seq = 0;
    local handled = false;
    if _nerf then
        local binding = DTHNHS[flag];
        if binding~=nil then
            local _sequence = NerfedMemory.GetBinding(binding);
            if _sequence then
                seq,atype = NerfedDecisions.Choose(_sequence);
                handled = true;
            end
        end
    end
    return seq,atype,handled;
end

function DetauntHelperNerf.GetBindingKey(modifierValue)
    return "DNerf"..modifierValue;
end

function DetauntHelperNerf.HandleNerfAddCommand(opt,input)
    local flag_type, remainder = input:match(L"([a-z]+)[ ]?(.*)");
    local flag_type = tostring(flag_type);
    local modifierValue = _SequenceTypes[flag_type];
    if modifierValue~=nil then
        local value = NerfedTalks.SequenceParsing(remainder);
        if value then
            local binding = DetauntHelperNerf.GetBindingKey(modifierValue)
            NerfedMemory.SetBinding(binding, value)
            DTHNHS[modifierValue]=binding;
            DetauntAbilityManager.ShowUI();
            return true;
        end
    end
    return false;
end


function DetauntHelperNerf.HandleNerfDelCommand(opt,input)
    local flag_type, remainder = input:match(L"([a-z]+)[ ]?(.*)");
    local flag_type = tostring(flag_type);
    local modifierValue = _SequenceTypes[flag_type];
    DetauntHelperNerf.NerfDel(modifierValue);
    return true;
end

function DetauntHelperNerf.NerfDel(modifierValue)
    if NerfedMemory then
        local binding = DetauntHelperNerf.GetBindingKey(modifierValue)    
        local cleared = NerfedMemory.ClearBinding(binding)
        if not cleared then
            d("Did not clear NB");
        end
        DTHNHS[modifierValue]=nil;        
        DetauntAbilityManager.ShowUI();
    end
end

function DetauntHelperNerf.HandleNerfListCommand(opt,input)
    local flag_type, remainder = input:match(L"([a-z]+)[ ]?(.*)");
    if flag_type~=nil then
        local flag_type = tostring(flag_type);
        local modifierValue = _SequenceTypes[flag_type];
        if modifierValue then
            local binding = DetauntHelperNerf.GetBindingKey(modifierValue)
            local _sequence = NerfedMemory.GetBinding(binding) or {}
            local str = towstring(_ReverseSequenceTypes[modifierValue].."->")..NerfedTalks.ListSequence(_sequence);
            TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, str)
            return true;
        end
    end
    local s = L"/dh list: ";
    local first = true;
    for k,v in pairs(DTHNHS) do
        if not first then
            s = s..L", ";
        else
            first = false;
        end
        s = s..towstring(_ReverseSequenceTypes[k]);
    end
    TextLogAddEntry("Chat", SystemData.ChatLogFilters.SAY, s)
    return true;
end

function DetauntHelperNerf.HandleNerfEditCommand(opt,input)
    local flag_type, remainder = input:match(L"([a-z]+)[ ]?(.*)");
    if flag_type~=nil then
        local flag_type = tostring(flag_type);
        local modifierValue = _SequenceTypes[flag_type];
        if modifierValue then
            local binding = DetauntHelperNerf.GetBindingKey(modifierValue)            
            local _sequence = NerfedMemory.GetBinding(binding) or {}
            local str = towstring("/dh add ".._ReverseSequenceTypes[modifierValue].." ")..NerfedTalks.ListSequence(_sequence);
            EA_ChatWindow.InsertText(str)
            return true;
        end
    end
end