<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="DetauntHelper" version="1.0" date="01/17/2009">
        <Author name="Shayme" email="none" />
        <Description text="Helps With Detaunting" />
        <VersionSettings gameVersion="1.3.3" />
        <Dependencies>       
            <Dependency name="TortallDPSCore" />
        </Dependencies>
        <Files>
            <File name="Source\TargetInfoFix.lua" />
            <File name="Source\Lang.lua" />
            <File name="Source\Common.lua" />
            <File name="Source\Core.xml" />
            <File name="Source\Core.lua" />
            <File name="Source\Bars.lua" />
            <File name="Source\Config\Abilities.xml" />
            <File name="Source\Config\Abilities.lua" />
            <File name="Source\Config\BarSettings.xml" />
            <File name="Source\Config\BarSettings.lua" />
            <File name="Source\Config\Targets.xml" />
            <File name="Source\Config\Targets.lua" />
            <File name="Source\Config\Config.xml" />
            <File name="Source\Config\Config.lua" />
            <File name="Source\Monitors\Monitor.xml" />
            <File name="Source\Monitors\Monitor.lua" />
            <File name="Source\Nerfed.lua" />
            <File name="Source\TargetInfo.lua" />
        </Files>
        <OnInitialize>
            <CreateWindow name="DetauntHelperIcon" />
            <CreateWindow name="DetauntHelperAnchorFrame" />
            <CreateWindow name="DetauntHelper_MonitorAnchorFrame" />
            <CallFunction name="DetauntHelper.Initialize" />
        </OnInitialize>
        <OnUpdate>
            <CallFunction name="DetauntHelper.OnUpdate" />
        </OnUpdate>
        <OnShutdown />
        <SavedVariables>
            <SavedVariable name="DetauntHelper.DetauntHelperSettings" />
            <SavedVariable name="DetauntHelper.TargetData" />
        </SavedVariables>
    </UiMod>
</ModuleFile>